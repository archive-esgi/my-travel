<?php

namespace App\Handler;

use App\Entity\Users;
use Core\Core;
use Core\HtmlRenderer;
use Mailer\Entity\Mail;
use Mailer\Mailer;

abstract class MailHandler
{
    public static function sendSignUp(Users $user): bool
    {
        $subject = Core::translate('mail', 'signup.subject', [
            "{siteName}" => Core::getConfig('site.name'),
        ]);

        $template = new HtmlRenderer('mail.signup');
        $template->add('title', $subject);
        $template->add(
            'body',
            Core::translate('mail', 'signup.content', [
                "{username}" => $user->getUsername(),
            ])
        );
        $template->add(
            'completeURL',
            "http://" . $_SERVER['HTTP_HOST'] .
            "/signup/complete?email=" . $user->getEmail() . "&token=" . $user->getToken()
        );

        $mail = new Mail(
            Core::getConfig('mailer.sender'),
            $subject,
            $template->render()
        );
        $mail->addReceiver($user->getEmail());
        return Mailer::send($mail);
    }

    public static function sendPasswordReset(Users $user): bool
    {
        $subject = Core::translate('mail', 'passwordReset.subject', [
            "{siteName}" => Core::getConfig('site.name'),
        ]);

        $template = new HtmlRenderer('mail.password_reset');
        $template->add('title', $subject);
        $template->add(
            'body',
            Core::translate('mail', 'passwordReset.content', [
                "{username}" => $user->getUsername(),
            ])
        );
        $template->add(
            'completeURL',
            "http://" . $_SERVER['HTTP_HOST'] .
            "/reset-password?email=" . $user->getEmail() . "&token=" . $user->getToken()
        );

        $mail = new Mail(
            Core::getConfig('mailer.sender'),
            $subject,
            $template->render()
        );
        $mail->addReceiver($user->getEmail());
        return Mailer::send($mail);
    }
}
