<?php

namespace App\Controller;

use Router\Entity\BaseController;
use Router\Entity\ViewResponse;
use Router\Router;
use App\Repository\TripsRepository;
use App\Repository\StepsRepository;
use App\Repository\UsersRepository;
use App\Repository\CommentsRepository;
use App\Repository\TagsRepository;
use App\Repository\TripsTagsRepository;
use Core\Core;
use Form\FormHandler;
use Form\FormValidator;

class TripController extends BaseController
{
    /**
     * Search action
     */
    public function search(): void
    {
        $tripsRepo = new TripsRepository();
        $tagsRepo = new TagsRepository();
        $tagId = $this->request->getQuery()["tag_id"];
        $searchQuery = $this->request->getRequest()["searchQuery"];

        $trips = [];
        $tag = null;

        if ($tagId) {
            $trips = $tripsRepo->getAllTripsUsingTagId($tagId);
            $tag = $tagsRepo->getOneTagBy([["id", "=", $tagId]]);

            if (!$trips) {
                Router::redirect('trip.search');
                return;
            }
        } else {
            $trips = $searchQuery ? $tripsRepo->getAllTripsUsingTagsLike($searchQuery)
                : $tripsRepo->getAllTrips('creation_date');
        }

        $view  = new ViewResponse('trip_search');

        if ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            $_SESSION["SUCCESS_MESSAGE"] = null;
        } elseif ($_SESSION["WELCOME_MESSAGE"]) {
            $view->add('welcomeMessage', $_SESSION["WELCOME_MESSAGE"]);
            $_SESSION["WELCOME_MESSAGE"] = null;
        }

        $view->add('trips', $trips);
        $view->add('searchQuery', $searchQuery);
        if ($tagId) {
            $view->add('tag', $tag);
        }
    }

    /**
     * Trip page
     */
    public function tripPage(): void
    {
        $tripsRepo = new TripsRepository();
        $stepsRepo = new StepsRepository();
        $usersRepo = new UsersRepository();
        $commentsRepo = new CommentsRepository();
        $tagsRepo = new TagsRepository();

        $queryData = $this->request->getQuery();
        $commentData = $this->request->getRequest();

        if (!is_numeric($queryData["trip_id"])) {
            Router::redirect('trip.search');
            return;
        }

        $trip = $tripsRepo->getOneTripBy([
            ["id", "=", $queryData["trip_id"]]
        ]);

        if (!$trip) {
            Router::redirect('trip.search');
            return;
        }

        // checking if there is a comment to insert or not with the length of the post attribute
        if (count($commentData) > 0) {
            $commentsRepo->postComment([
                "text" => $commentData["comment"],
                "tripId" => $trip->getId(),
                "userId" => Core::getCurrentUser()->getId()
            ]);
            $_SESSION["SUCCESS_MESSAGE"] = "commentCreatedSuccess";
        }

        $usedTags = $tagsRepo->getTagsUsedByTrip($trip->getId());

        $steps = $stepsRepo->getAllStepsBy([
            ["trip_id", "=", $trip->getId()]
        ]);

        $comments = $commentsRepo->getAllCommentsBy([
            ["trip_id", "=", $trip->getId()]
        ]);

        usort($comments, function ($commentA, $commentB) {
            return $commentA->getCreationDate()->getTimestamp() > $commentB->getCreationDate()->getTimestamp();
        });

        $author = $usersRepo->getOneUserBy([
            ["id", "=", $trip->getUserId()]
        ]);

        $view  = new ViewResponse('trip');

        if ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            $_SESSION["SUCCESS_MESSAGE"] = null;
        }

        $view->add('usedTags', $usedTags);
        $view->add('steps', $steps);
        $view->add('trip', $trip);
        $view->add('author', $author);
        $view->add('comments', $comments);
    }

    /**
     * Show the create trip form
     */
    public function showCreateTripForm(): void
    {
        $view = new ViewResponse('user_create_trip');
        $view->add('createTripForm', FormHandler::getForm('createTrip')->render());
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            $view->add('form_params', $_SESSION["FORM_PARAMS"]);
            unset($_SESSION["FORM_PARAMS"]);
            $_SESSION["ERROR_MESSAGE"] = null;
        }
    }

    /**
     * Create trip
     */
    public function createTrip(): void
    {
        $tripsRepo = new TripsRepository();
        $createTripFormData = $this->request->getRequest();

        // checking if there is a trip to insert or not with the length of the post attribute
        if (count($createTripFormData) == 0) {
            Router::redirect('user.show_create_trip_form');
            return;
        }

        $createTripFormData['image'] = $this->request->getFiles()['image'];
        $formValidation = FormValidator::validateForm('createTrip', $createTripFormData);

        // Validation check
        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        }

        $startingDate = new \DateTime($createTripFormData["starting-date"]);
        $endingDate = !empty($createTripFormData["ending-date"]) ?
            new \DateTime($createTripFormData["ending-date"]) : null;
        // checkin if ending date is after the starting date, otherwise we generate an error
        if ($endingDate && $endingDate->getTimestamp() <= $startingDate->getTimestamp()) {
            $_SESSION["ERROR_MESSAGE"] = "endDateIncorrect";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            $_SESSION["FORM_PARAMS"] = $createTripFormData;
            Router::redirect('user.show_create_trip_form');
            return;
        }

        $imageId = explode("/", $createTripFormData["image"]["tmp_name"])[2];
        $imageName = $imageId . '_' . $createTripFormData['image']['name'];

        FormHandler::uploadFile($createTripFormData['image'], $imageName, '../../public/assets/images/upload/trips');

        $createdTripId = $tripsRepo->createTrip([
            "title" => $createTripFormData["title"],
            "description" => $createTripFormData["description"],
            "imagePath" => '/assets/images/upload/trips/' . $imageName,
            "startingDate" => $createTripFormData["starting-date"],
            "endingDate" => $createTripFormData["ending-date"],
            "userId" => Core::getCurrentUser()->getId()
        ]);

        // tags
        $tagsRepo = new TagsRepository();
        $tripsTagsRepo = new TripsTagsRepository();

        $tags = explode(';', $createTripFormData['tags']);
        foreach ($tags as $tag) {
            $tagLabel = strtolower(trim($tag));

            if (strlen($tagLabel) > 0) {
                // Checking if a corresponding tag already exists
                $createdTag = $tagsRepo->getOneTagBy([
                    ["label", "=", $tagLabel]
                ]);

                // If there is no tag, we create it
                if (!$createdTag) {
                    $createdTag = $tagsRepo->createTag([
                        'label' => $tagLabel
                    ]);
                }

                // Making sure the user did not put the same tag twice in the input
                if (count($tripsTagsRepo->getAllTripsTagsBy([
                        ["trip_id", "=", $createdTripId],
                        ["tag_id", "=", $createdTag->getId()],])) == 0) {
                    $tripsTagsRepo->createTripTag([
                        "tripId" => $createdTripId,
                        "tagId" => $createdTag->getId()
                    ]);
                }
            }
        }

        $_SESSION['SUCCESS_MESSAGE'] = 'tripCreatedSuccess';
        Router::redirect('trip.search');
    }

    /**
     * Delete trip
     */
    public function deleteTrip()
    {
        $tripsRepo = new TripsRepository();
        $tripsRepo->deleteTrip([
            ["id", "=", intval($this->request->getRequest()['trip-id'])]
        ]);

        $_SESSION['SUCCESS_MESSAGE'] = 'tripDeletedSuccess';
        Router::redirect('user.trips');
    }

    /**
     * Show edit trip form (page is also containing the edit steps forms)
     */
    public function showEditTripForm()
    {
        $queryData = $this->request->getQuery();
        $tripsRepo = new TripsRepository();
        $stepsRepo = new StepsRepository();
        $tagsRepo = new TagsRepository();

        $trip = $tripsRepo->getOneTripBy([["id", "=", intval($queryData["trip_id"])]]);
        $steps = $stepsRepo->getAllStepsBy([["trip_id", "=", $trip->getId()]], 'creation_date');

        $tagsLabels = array_map(function ($tag) {
            return $tag->getLabel();
        }, $tagsRepo->getTagsUsedByTrip($trip->getId()));

        $tagsLabelsImploded = implode(';', $tagsLabels);

        $view = new ViewResponse('user_edit_trip');

        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            $_SESSION["ERROR_MESSAGE"] = null;
        } elseif ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            $_SESSION["SUCCESS_MESSAGE"] = null;
        }

        $view->add('trip', $trip);
        $view->add('steps', $steps);
        $view->add('tagsLabelsImploded', $tagsLabelsImploded);
    }

    /**
     * Edit trip
     * data keys are separated with '-' because the page also contains the edit steps forms
     */
    public function editTrip()
    {
        $tripsRepo = new TripsRepository();
        $stepsRepo = new StepsRepository();
        $editTripFormData = $this->request->getRequest();
        $editTripFormData['image'] = $this->request->getFiles()['trip-image'];
        $tripToUpdate = $tripsRepo->getOneTripBy([['id', '=', $editTripFormData['trip-id']]]);

        $startingDate = new \Datetime($editTripFormData["trip-starting-date"]);

        $endingDate = !empty($editTripFormData["trip-ending-date"]) ?
            new \DateTime($editTripFormData["trip-ending-date"]) : null;
        $tripStepsByStartingDate = $stepsRepo->
        getAllStepsBy([["trip_id", "=", $editTripFormData['trip-id']]], 'starting_date');
        $tripStepsByEndingDate = $stepsRepo->
        getAllStepsBy([["trip_id", "=", $editTripFormData['trip-id']]], 'ending_date desc');

        if (!empty($tripStepsByStartingDate) && $startingDate->
            getTimestamp() > $tripStepsByStartingDate[0]->getStartingDate()->getTimestamp()) {
            $_SESSION["ERROR_MESSAGE"] = "dateTripAfterStartDateStep";
        } elseif ($endingDate && $endingDate->getTimestamp() <= $startingDate->getTimestamp()) {
            $_SESSION["ERROR_MESSAGE"] = "dateTripBeforStartDateTrip";
        } elseif (!empty($tripStepsByEndingDate) && $endingDate
            && $endingDate->getTimestamp() < $tripStepsByEndingDate[0]->getEndingDate()->getTimestamp()) {
            $_SESSION["ERROR_MESSAGE"] = "dateTripBeforEndDateStep";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('/user/edit-trip?trip_id=' . $editTripFormData['trip-id']);
            return;
        }

        if ($editTripFormData['image']['tmp_name']) {
            $imageId = explode("/", $editTripFormData["image"]["tmp_name"])[2];
            $imageName = $imageId . '_' . $editTripFormData['image']['name'];

            FormHandler::uploadFile($editTripFormData['image'], $imageName, '../../public/assets/images/upload/trips');
            $editTripFormData['trip-image-path'] = '/assets/images/upload/trips/' . $imageName;
        } else {
            $editTripFormData['trip-image-path'] = $tripToUpdate->getImagePath();
        }

        // tags edit
        $tagsRepo = new TagsRepository();
        $tripsTagsRepo = new TripsTagsRepository();
        $editedTags = explode(';', $editTripFormData['trip-tags']);

        // Get the existing tripsTags of the trip  to check if we have to delete some
        // in case the user deleted some tags that has existed in the trip before
        $existingTripsTags = $tripsTagsRepo->getAllTripsTagsBy([["trip_id", "=", $editTripFormData['trip-id']]]);

        foreach ($existingTripsTags as $existingTripTag) {
            $existingTag = $tagsRepo->getOneTagBy([["id", "=", $existingTripTag->getTagId()]]);
            $existingTagLabel = $existingTag->getLabel();
            $found = false;

            foreach ($editedTags as $tag) {
                $tagLabel = strtolower(trim($tag));
                if ($existingTagLabel == $tagLabel) {
                    $found = true;
                }
            }

            if (!$found) {
                // The user deleted an existing tag for the trip, we have to delete the corrseponding TripTag
                $tripsTagsRepo->deleteTripTag([["id", "=", $existingTripTag->getId()]]);
            }
        }

        foreach ($editedTags as $tag) {
            $tagLabel = strtolower(trim($tag));

            if (strlen($tagLabel) > 0) {
                // Checking if a corresponding tag already exists
                $createdTag = $tagsRepo->getOneTagBy([
                    ["label", "=", $tagLabel]
                ]);

                // If there is no tag, we create it
                if (!$createdTag) {
                    $createdTag = $tagsRepo->createTag([
                        'label' => strtolower($tagLabel)
                    ]);
                }

                // Making sure the user did not put the same tag twice in the input
                if (count($tripsTagsRepo->getAllTripsTagsBy([
                        ["trip_id", "=", $editTripFormData['trip-id']],
                        ["tag_id", "=", $createdTag->getId()]])) == 0) {
                    $tripsTagsRepo->createTripTag([
                        "tripId" => $editTripFormData['trip-id'],
                        "tagId" => $createdTag->getId()
                    ]);
                }
            }
        }

        $tripsRepo->editTrip($editTripFormData);
        $_SESSION["SUCCESS_MESSAGE"] = "tripModified";
        Router::redirect('/user/edit-trip?trip_id=' . $editTripFormData['trip-id']);
    }
}
