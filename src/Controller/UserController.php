<?php

namespace App\Controller;

use App\Handler\MailHandler;
use Core\Core;
use Router\Entity\BaseController;
use Router\Entity\ViewResponse;
use Form\FormHandler;
use Form\FormValidator;
use Router\Router;
use App\Entity\Users;
use ORM\ORMHandler;
use App\Repository\TripsRepository;
use App\Repository\StepsRepository;
use Core\CurrentUser;

class UserController extends BaseController
{
    /**
     * Show signin form action
     */
    public function showSigninForm(): void
    {
        $view  = new ViewResponse('signin');
        $view->add('signinForm', FormHandler::getForm('signin')->render());
        $view->add('forgottenPasswordForm', FormHandler::getForm('forgotten_password')->render());
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        } elseif ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            unset($_SESSION["SUCCESS_MESSAGE"]);
        }
    }

    /**
     * Signin action
     */
    public function signin(): void
    {
        $signinFormData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('signin', $signinFormData);

        // Form errors
        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
            Router::redirect('signin');
            return;
        }

        $userToSignin = new Users();
        $userToSignin->setEmail($signinFormData["email"]);
        $userToSignin->setRawPassword($signinFormData["password"]);

        // errors on login, it is empty if the login went well
        $loginError = $userToSignin->login()[0];

        if ($loginError) {
            $_SESSION["ERROR_MESSAGE"] = $loginError;
            Router::redirect('signin');
            return;
        }

        $_SESSION["WELCOME_MESSAGE"] = Core::translate('home', 'welcome')
            . CurrentUser::getCurrentUser()->getFirstname();
        // If we reach this code, the login is good and we redirect to trip-search page
        Router::redirect('trip.search', 200);
    }

    /**
     * Show signup form action
     */
    public function showSignupForm(): void
    {
        $view  = new ViewResponse('signup');
        $view->add('signupForm', FormHandler::getForm('signup')->render());
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            $view->add('form_params', $_SESSION["FORM_PARAMS"]);
            unset($_SESSION["FORM_PARAMS"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        } elseif ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            unset($_SESSION["SUCCESS_MESSAGE"]);
        }
    }

    /**
     * Show signup complete
     */
    public function signupComplete(): void
    {
        $view  = new ViewResponse('signup_complete');

        $email = $this->request->getQuery()['email'];
        $token = $this->request->getQuery()['token'];

        if (isset($email) && isset($token)) {
            $userNotComplete = ORMHandler::getOneEntityBy(new Users(), [
                ["email", "=", $email],
                ["token", "=", $token],
                ["status", "=", 0],
            ]);

            if ($userNotComplete) {
                /** @var $userNotComplete Users */
                $userNotComplete->setStatus(1);
                $userNotComplete->cleanToken();
                $userNotComplete->save();
                $userNotComplete->login();
                $_SESSION["SUCCESS_MESSAGE"] =
                    "Votre inscription a bien été finalisé, il ne vous reste plus qu'à vous connecter !";
            } else {
                $_SESSION["ERROR_MESSAGE"] = "Utilisateur inconnu";
            }
        } else {
            $_SESSION["ERROR_MESSAGE"] = "Informations inexistantes";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        } elseif ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            unset($_SESSION["SUCCESS_MESSAGE"]);
        }
    }

    /*
     * Signup action
     */
    public function signup(): void
    {
        $signupFormData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('signup', $signupFormData);

        // If there is an existing user with the form email, we can't save it
        $existingUserWithEmail = ORMHandler::getOneEntityBy(new Users(), [
            ["email", "=", $signupFormData["email"]]
        ]);

        // Validation check, we redirect to signup page with the correct error message if it failed
        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        } elseif ($signupFormData["password"] != $signupFormData["confirm_password"]) {
            $_SESSION["ERROR_MESSAGE"] = "Mots de passes différents";
        } elseif ($existingUserWithEmail) {
            $_SESSION["ERROR_MESSAGE"] = "Adresse email déjà utilisée";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            $_SESSION["FORM_PARAMS"] = $signupFormData;
            Router::redirect('signup');
            return;
        }

        $userToSignup = new Users();
        $userToSignup->setEmail($signupFormData["email"]);
        $userToSignup->setRawPassword($signupFormData["password"]);
        $userToSignup->setFirstname($signupFormData["firstname"]);
        $userToSignup->setLastname($signupFormData["lastname"]);
        $userToSignup->setUsername($signupFormData["username"]);
        $userToSignup->setRole('user');

        // Check if a mail must be send to sign up
        if (Core::getConfig('signup.sendMail')) {
            $userToSignup->setStatus(0);
            $userToSignup->generateToken();

            // Send the mail
            if (!MailHandler::sendSignUp($userToSignup)) {
                $_SESSION["ERROR_MESSAGE"] = "Erreur lors de l'envoi de l'email de finalisation.
                                                Merci de nous contacter.";
                $_SESSION["FORM_PARAMS"] = $signupFormData;
                Router::redirect('signup');
                return;
            }

            // Saving new user to db
            $userToSignup->save();

            $_SESSION["SUCCESS_MESSAGE"] = "Un email vous a été envoyé à l'adresse " . $userToSignup->getEmail() .
                " afin de compléter votre inscription !";

            // Redirect user to trips page
            Router::redirect('signup', 200);
        } else {
            $userToSignup->setStatus(1);

            // Saving new user to db
            $userToSignup->save();
            // Login new user directly after
            $userToSignup->login();
            // Redirect user to trips page
            $_SESSION["WELCOME_MESSAGE"] = Core::translate('home', 'welcome') . $userToSignup->getFirstname();
            Router::redirect('trip.search', 200);
        }
    }

    /*
     * Disconnect action
     */
    public function disconnect(): void
    {
        Users::disconnect();
        Router::redirect('home', 200);
    }

    /*
     * Send mail on forgotten password request
     */
    public function forgottenPassword(): void
    {
        $forgottenPasswordFormData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('forgotten_password', $forgottenPasswordFormData);

        // Form errors
        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
            Router::redirect('signin');
            return;
        }


        if (isset($forgottenPasswordFormData['email'])) {
            $user = ORMHandler::getOneEntityBy(new Users(), [
                ["email", "=", $forgottenPasswordFormData['email']],
                ["status", "=", 1],
            ]);

            if ($user) {
                $user->generateToken();
                $user->save();
                if (!MailHandler::sendPasswordReset($user)) {
                    $_SESSION["ERROR_MESSAGE"] =
                        Core::translate('user.forgotten_password', 'error.sendFail');
                    Router::redirect('signin', 200);
                }
            }

            $_SESSION["SUCCESS_MESSAGE"] =
                Core::translate('user.forgotten_password', 'success.mailSent');
            Router::redirect('signin', 200);
        } else {
            $_SESSION["ERROR_MESSAGE"] =
                Core::translate('user.forgotten_password', 'error.emailNotFound');
            Router::redirect('signin', 200);
        }
    }

    // Complete forgotten password request
    public function forgottenPasswordComplete(): void
    {
        $view  = new ViewResponse('password_reset');

        if (isset($this->request->getQuery()['email']) && isset($this->request->getQuery()['token'])) {
            $_SESSION["PASSWORD_RESET_EMAIL"] = $this->request->getQuery()['email'];
            $_SESSION["PASSWORD_RESET_TOKEN"] = $this->request->getQuery()['token'];
        }

        if (isset($_SESSION["PASSWORD_RESET_EMAIL"]) && isset($_SESSION["PASSWORD_RESET_TOKEN"])) {
            $email = $_SESSION["PASSWORD_RESET_EMAIL"];
            $token = $_SESSION["PASSWORD_RESET_TOKEN"];
        }

        if (isset($email) && isset($token)) {
            $user = ORMHandler::getOneEntityBy(new Users(), [
                ["email", "=", $email],
                ["token", "=", $token],
                ["status", "=", 1],
            ]);

            if ($user) {
                $view->add('passwordResetForm', FormHandler::getForm('password_reset')->render());
                if (isset($this->request->getRequest()['sent'])) {
                    $this->passwordReset($user);
                }
            } else {
                $_SESSION["ERROR_MESSAGE"] = "Utilisateur inconnu";
            }
        } else {
            if (empty($this->request->getRequest()['sent'])) {
                $_SESSION["ERROR_MESSAGE"] = "Informations inexistantes";
            }
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        } elseif ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            unset($_SESSION["SUCCESS_MESSAGE"]);
        }
    }

    /*
     * Reset password
     */
    private function passwordReset(Users $user) : void
    {
        $passwordResetFormData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('password_reset', $passwordResetFormData);

        // Validation check, we redirect to signup page with the correct error message if it failed
        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        } elseif ($passwordResetFormData["password"] != $passwordResetFormData["confirm_password"]) {
            $_SESSION["ERROR_MESSAGE"] = "Mots de passes différents";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            return;
        }

        $user->setRawPassword($passwordResetFormData["password"]);
        $user->cleanToken();

        // Saving new user to db
        $user->save();

        unset($_SESSION["PASSWORD_RESET_EMAIL"]);
        unset($_SESSION["PASSWORD_RESET_TOKEN"]);
        // Redirect user to trips page
        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('user.forgotten_password', 'success.resetPassword');
    }

    /**
     * User settings account information's
     */
    public function settingsGeneral(): void
    {
        $view = new ViewResponse('user_settings_general');

        $currentUser = Core::getCurrentUser();

        $view->add('lastname', $currentUser->getLastname());
        $view->add('firstname', $currentUser->getFirstname());
        $view->add('username', $currentUser->getUsername());
        $view->add('email', $currentUser->getEmail());
        $view->add('description', $currentUser->getDescription());

        $view->add('settingsGeneralForm', FormHandler::getForm('user_settings_general')->render());
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        } elseif ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            unset($_SESSION["SUCCESS_MESSAGE"]);
        }
    }

    /**
     * User settings account information's check
     */
    public function settingsGeneralCheck(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('user_settings_general', $formData);

        $currentUser = Core::getCurrentUser();

        // If there is an existing user with the form email, we can't save it
        $existingUserWithEmail = ORMHandler::getOneEntityBy(new Users(), [
            ["email", "=", $formData["email"]]
        ]);

        // Validation check, we redirect to signup page with the correct error message if it failed
        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        } elseif ($existingUserWithEmail && $existingUserWithEmail->getId() != $currentUser->getId()) {
            $_SESSION["ERROR_MESSAGE"] = "emailUsed";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('user.settings.general');
            return;
        }

        $profilePicture = $this->request->getFiles()['profile_picture'];

        if ($profilePicture['name'] != "") {
            $currentUser->replaceProfileImg($profilePicture);
        }

        $currentUser->setFirstname($formData["firstname"]);
        $currentUser->setLastname($formData["lastname"]);
        $currentUser->setUsername($formData["username"]);
        $currentUser->setEmail($formData["email"]);
        $currentUser->setDescription($formData["description"]);

        $currentUser->save();
        // Recreating the login cookie with the new email if it has been changed
        $currentUser->createAutoLoginCookie(CurrentUser::generateCurrentUser());

        $_SESSION["SUCCESS_MESSAGE"] = "successSaveInformation";

        Router::redirect('user.settings.general', 200);
    }
    
    /**
     * User settings change password
     */
    public function settingsPassword(): void
    {
        $view = new ViewResponse('user_settings_password');
        $view->add('settingsPasswordForm', FormHandler::getForm('user_settings_password')->render());
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        } elseif ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            unset($_SESSION["SUCCESS_MESSAGE"]);
        }
    }

    /**
     * User settings change password check
     */
    public function settingsPasswordCheck(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('user_settings_password', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        } elseif (password_verify($formData["current_password"], Core::getCurrentUser()->getPassword()) != true) {
            $_SESSION["ERROR_MESSAGE"] = "currentPwdIncorrect";
        } elseif ($formData["new_password"] != $formData["confirm_new_password"]) {
            $_SESSION["ERROR_MESSAGE"] = "newPwdDifferent";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('user.settings.password');
            return;
        }

        $currentUser = Core::getCurrentUser();
        $currentUser->setRawPassword($formData["new_password"]);
        $currentUser->save();

        $_SESSION["SUCCESS_MESSAGE"] = "passwordChanged";

        Router::redirect('user.settings.password', 200);
    }

    /**
     * User trips page
     */
    public function userTrips(): void
    {
        $tripsRepo = new TripsRepository();
        $stepsRepo = new StepsRepository();

        $trips = $tripsRepo->getAllTripsBy([
            ['user_id', '=', Core::getCurrentUser()->getId()]
        ], 'creation_date asc');

        $view = new ViewResponse('user_trips');

        $stepsCount = 0;
        foreach ($trips as $trip) {
            $tripSteps = $stepsRepo->getAllStepsBy([["trip_id", "=", $trip->getId()]]);
            $stepsCount += count($tripSteps);
            ${'tripsSteps' . $trip->getId()} = $tripSteps;
            $view->add('tripsSteps' . $trip->getId(), ${'tripsSteps' . $trip->getId()});
        }

        if ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            $_SESSION["SUCCESS_MESSAGE"] = null;
        }

        $view->add('trips', $trips);
        $view->add('stepsCount', $stepsCount);
    }
}
