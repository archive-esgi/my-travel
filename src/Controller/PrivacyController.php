<?php

namespace App\Controller;

use Router\Entity\BaseController;
use Router\Entity\ViewResponse;

class PrivacyController extends BaseController
{
    /**
     *  Privacy action
     */
    public function privacy(): void
    {
        $view  = new ViewResponse('privacy');
    }
}
