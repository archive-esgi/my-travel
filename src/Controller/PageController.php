<?php

namespace App\Controller;

use Router\Entity\BaseController;
use Router\Entity\ViewResponse;
use Router\Router;
use App\Repository\PagesRepository;

class PageController extends BaseController
{
    public function showPage(): void
    {
        $pagesRepo = new PagesRepository();
        $slugData = $this->request->getQuery()['slug'];

        if (!$slugData || empty(trim($slugData))) {
            Router::redirect('home');
        }

        $page = $pagesRepo->getOnePageBy([['slug', '=' , $slugData]]);

        if ($page) {
            $view  = new ViewResponse('custom_page');
            $view->add('pageTitle', $page->getTitle());
            $view->add('pageContent', $page->getContent());
        } else {
            Router::redirect('home');
        }
    }
}
