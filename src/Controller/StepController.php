<?php

namespace App\Controller;

use Router\Entity\BaseController;
use Router\Entity\ViewResponse;
use Router\Router;
use App\Repository\StepsRepository;
use App\Repository\TripsRepository;
use Form\FormHandler;
use Form\FormValidator;

class StepController extends BaseController
{
    /**
     * Show the create step form
     */
    public function showCreateStepForm(): void
    {
        $tripId = $this->request->getQuery()['trip_id'];

        // Checking if we have a trip_id or not, otherwise we can't insert a step and have to redirect the user
        if (!$tripId || !is_numeric($tripId)) {
            Router::redirect('trip.search');
            return;
        }

        $view = new ViewResponse('user_create_step');
        $view->add('createStepForm', FormHandler::getForm('createStep')->render());
        $view->add('tripId', $tripId);
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            $view->add('form_params', $_SESSION["FORM_PARAMS"]);
            unset($_SESSION["FORM_PARAMS"]);
            $_SESSION["ERROR_MESSAGE"] = null;
        }
    }

    /**
     * Create step
     */
    public function createStep(): void
    {
        $stepsRepo = new StepsRepository();
        $tripsRepo = new TripsRepository();
        $createStepFormData = $this->request->getRequest();

        $trip = $tripsRepo->getOneTripBy([
            ["id", "=", $createStepFormData["trip-id"]]
        ]);

        // If no data, we redirect
        if (count($createStepFormData) == 0 || !$trip) {
            Router::redirect('trip.search');
            return;
        }

        $createStepFormData['image'] = $this->request->getFiles()['image'];
        $formValidation = FormValidator::validateForm('createStep', $createStepFormData);

        // Validation check
        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        }

        $startingDate = new \DateTime($createStepFormData["starting-date"]);
        $endingDate = !empty($createStepFormData["ending-date"]) ?
            new \DateTime($createStepFormData["ending-date"]) : null;
        // dates checks
        if ($endingDate && $endingDate->getTimestamp() <= $startingDate->getTimestamp()) {
            $_SESSION["ERROR_MESSAGE"] = "endDateStepBeforStartDateStep";
        } elseif ($startingDate->getTimestamp() < $trip->getStartingDate()->getTimestamp()) {
            $_SESSION["ERROR_MESSAGE"] = "startDateStepBeforStartDateTrip";
        } elseif ($endingDate && $trip->getEndingDate() && $endingDate > $trip->getEndingDate()) {
            $_SESSION["ERROR_MESSAGE"] = "endDateStepAfterStartDateTrip";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            $_SESSION["FORM_PARAMS"] = $createStepFormData;
            Router::redirect('/user/create-step?trip_id=' . $createStepFormData["trip-id"]);
            return;
        }

        $imageId = explode("/", $createStepFormData["image"]["tmp_name"])[2];
        $imageName = $imageId . '_' . $createStepFormData['image']['name'];

        FormHandler::uploadFile($createStepFormData['image'], $imageName, '../../public/assets/images/upload/steps');

        $stepsRepo->createStep([
            "title" => $createStepFormData["title"],
            "description" => $createStepFormData["description"],
            "imagePath" => '/assets/images/upload/steps/' . $imageName,
            "longitude" => $createStepFormData["step-lng"],
            "latitude" => $createStepFormData["step-lat"],
            "startingDate" => $createStepFormData["starting-date"],
            "endingDate" => $createStepFormData["ending-date"],
            "tripId" => $createStepFormData["trip-id"]
        ]);

        $_SESSION['SUCCESS_MESSAGE'] = 'stepCreated';
        Router::redirect('/trip?trip_id=' . $trip->getId());
    }

    /**
     * Edit step
     * data keys are separated with '-' because the page also contains the edit trip form
     */
    public function editStep()
    {
        $stepsRepo = new StepsRepository();
        $tripsRepo = new TripsRepository();
        $editStepFormData = $this->request->getRequest();
        $editStepFormData['image'] = $this->request->getFiles()['image'];

        $trip = $tripsRepo->getOneTripBy([
            ["id", "=", $editStepFormData["trip-id"]]
        ]);

        // If no data, we redirect
        if (count($editStepFormData) == 0 || !$trip) {
            Router::redirect('trip.search');
            return;
        }

        $startingDate = new \DateTime($editStepFormData["starting-date"]);

        $endingDate = !empty($editStepFormData["ending-date"]) ?
            new \DateTime($editStepFormData["ending-date"]) : null;

        // dates checks
        if ($endingDate && $endingDate->getTimestamp() <= $startingDate->getTimestamp()) {
            $_SESSION["ERROR_MESSAGE"] = "endDateStepBeforStartDateStep";
        } elseif ($endingDate && $trip->getEndingDate() && $endingDate > $trip->getEndingDate()) {
            $_SESSION["ERROR_MESSAGE"] = "endDateStepAfterStartDateTrip";
        } elseif ($startingDate->getTimestamp() < $trip->getStartingDate()->getTimestamp()) {
            $_SESSION["ERROR_MESSAGE"] = "startDateStepBeforStartDateTrip";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('/user/edit-trip?trip_id=' . $editStepFormData['trip-id']);
            return;
        }

        if ($editStepFormData['image']['tmp_name']) {
            $imageId = explode("/", $editStepFormData["image"]["tmp_name"])[2];
            $imageName = $imageId . '_' . $editStepFormData['image']['name'];

            FormHandler::uploadFile($editStepFormData['image'], $imageName, '../../public/assets/images/upload/steps');
            $editStepFormData['image-path'] = '/assets/images/upload/steps/' . $imageName;
        } else {
            $stepToUpdate = $stepsRepo->getOneStepBy([
                ["id", "=", $editStepFormData["step-id"]]
            ]);

            $editStepFormData['image-path'] = $stepToUpdate->getImagePath();
        }

        $stepsRepo->editStep($editStepFormData);
        $_SESSION["SUCCESS_MESSAGE"] = "stepModified";
        Router::redirect('/user/edit-trip?trip_id=' . $editStepFormData['trip-id']);
    }

    /*
     * Delete a step
     */
    public function deleteStep(): void
    {
        $stepId = $this->request->getRequest()['step-id'];
        $tripId = $this->request->getRequest()['trip-id'];

        if ($stepId) {
            $stepsRepo = new StepsRepository();
            $stepsRepo->deleteStep([
                ["id", "=", $stepId]
            ]);
            $_SESSION["SUCCESS_MESSAGE"] = "stepDeleted";
        }
        Router::redirect('/user/edit-trip?trip_id=' . $tripId);
    }
}
