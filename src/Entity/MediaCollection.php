<?php

namespace App\Entity;

use ORM\Entity\BaseEntityCollection;

class MediaCollection extends BaseEntityCollection
{
    public function __construct(array $entities = [])
    {
        parent::__construct($entities);
    }
}
