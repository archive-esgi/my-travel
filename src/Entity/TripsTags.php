<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class TripsTags extends BaseEntity
{
    protected $trip_id;
    protected $tag_id;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get trip id.
     *
     * @return int|null
     */
    public function getTripId(): ?int
    {
        return $this->trip_id;
    }

    /**
     * Set trip id.
     *
     * @param int $trip_id
     * @return TripsTags
     */
    public function setTripId(int $trip_id): TripsTags
    {
        $this->trip_id = $trip_id;

        return $this;
    }

    /**
     * Get tag id.
     *
     * @return int|null
     */
    public function getTagId(): ?int
    {
        return $this->tag_id;
    }

    /**
     * Set tag id.
     *
     * @param int $tag_id
     * @return TripsTags
     */
    public function setTagId(int $tag_id): TripsTags
    {
        $this->tag_id = $tag_id;

        return $this;
    }
}
