<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Pages extends BaseEntity
{
    protected $name;
    protected $title;
    protected $slug;
    protected $content;
    protected $location;
    protected $creation_date;
    protected $update_date;

    public function __construct()
    {
        parent::__construct();
        $this->setIgnoredFields(['creation_date']);
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     * @return Pages
     */
    public function setName(string $name): Pages
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     * @return Pages
     */
    public function setTitle(string $title): Pages
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     * @return Pages
     */
    public function setSlug(string $slug): Pages
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Set content.
     *
     * @param string $content
     * @return Pages
     */
    public function setContent(string $content): Pages
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get location.
     *
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * Set location.
     *
     * @param string $location
     * @return Pages
     */
    public function setLocation(string $location): Pages
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get creation date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getCreationDate(): \DateTime
    {
        return new \DateTime($this->creation_date);
    }

    /**
     * Set creation date.
     *
     * @param \DateTime $creation_date
     * @return Pages
     */
    public function setCreationDate(\DateTime $creation_date): Pages
    {
        $this->creation_date = $creation_date->getTimestamp();
        return $this;
    }

    /**
     * Get update date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getUpdateDate(): \DateTime
    {
        return new \DateTime($this->update_date);
    }

    /**
     * Set update date.
     *
     * @param \DateTime $update_date
     * @return Pages
     */
    public function setUpdateDate(\DateTime $update_date): Pages
    {
        $this->update_date = $update_date->getTimestamp();

        return $this;
    }
}
