<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Steps extends BaseEntity
{
    protected $trip_id;
    protected $title;
    protected $description;
    protected $longitude;
    protected $latitude;
    protected $starting_date;
    protected $ending_date;
    protected $creation_date;
    protected $image_path;

    public function __construct()
    {
        parent::__construct();
        $this->setIgnoredFields(['creation_date']);
    }

    /**
     * Get trip id.
     *
     * @return int|null
     */
    public function getTripId(): ?int
    {
        return $this->trip_id;
    }

    /**
     * Set trip id.
     *
     * @param int $trip_id
     * @return Steps
     */
    public function setTripId(int $trip_id): Steps
    {
        $this->trip_id = $trip_id;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     * @return Steps
     */
    public function setTitle(string $title): Steps
    {
        $this->title = htmlspecialchars($title);

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     * @return Steps
     */
    public function setDescription(string $description): Steps
    {
        $this->description = htmlspecialchars($description);

        return $this;
    }

    /**
     * Get longitude.
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set longitude.
     *
     * @param string $longitude
     * @return Steps
     */
    public function setLongitude(string $longitude): Steps
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get latitude.
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set latitude.
     *
     * @param string $latitude
     * @return Steps
     */
    public function setLatitude(string $latitude): Steps
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get starting date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getStartingDate(): \DateTime
    {
        return new \DateTime($this->starting_date);
    }

    /**
     * Set starting date.
     *
     * @param $starting_date
     * @return Steps
     */
    public function setStartingDate($starting_date): Steps
    {
        if (gettype($starting_date) == 'string') {
            $this->starting_date = $starting_date;
        } else {
            $this->starting_date = $starting_date->getTimestamp();
        }
        return $this;
    }

    /**
     * Get ending date.
     *
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getEndingDate(): ?\Datetime
    {
        return isset($this->ending_date) ? new \DateTime($this->ending_date) : null;
    }

    /**
     * Set ending date.
     *
     * @param $ending_date
     * @return Steps
     */
    public function setEndingDate($ending_date): Steps
    {
        if ($ending_date == null) {
            $this->ending_date = null;
        } elseif (gettype($ending_date) == 'string') {
            $this->ending_date = $ending_date;
        } else {
            $this->ending_date = $ending_date->getTimestamp();
        }

        return $this;
    }

    /**
     * Get image_path.
     *
     * @return string|null
     */
    public function getImagePath(): ?string
    {
        return $this->image_path;
    }

    /**
     * Set image_path.
     *
     * @param string $image_path
     * @return Steps
     */
    public function setImagePath(string $image_path): Steps
    {
        $this->image_path = $image_path;

        return $this;
    }

    /**
     * Get creation date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getCreationDate(): \DateTime
    {
        return new \DateTime($this->creation_date);
    }

    /**
     * Set creation date.
     *
     * @param \DateTime $creation_date
     * @return Steps
     */
    public function setCreationDate(\DateTime $creation_date): Steps
    {
        $this->creation_date = $creation_date->getTimestamp();
        return $this;
    }

    /**
     * Get description excerpt.
     *
     * @return string
     */
    public function getExcerpt(): ?string
    {
        return strlen($this->getDescription()) > 150 ?
            substr($this->getDescription(), 0, 150) . '...' : $this->getDescription();
    }
}
