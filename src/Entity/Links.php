<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Links extends BaseEntity
{
    protected $location_id;
    protected $name;
    protected $url;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get location id.
     *
     * @return int|null
     */
    public function getLocationId(): ?int
    {
        return $this->location_id;
    }

    /**
     * Set location id.
     *
     * @param int $location_id
     * @return Links
     */
    public function setLocationId(int $location_id): Links
    {
        $this->location_id = $location_id;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     * @return Links
     */
    public function setName(string $name): Links
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get URL.
     *
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * Set URL.
     *
     * @param string $url
     * @return Links
     */
    public function setUrl(string $url): Links
    {
        $this->url = $url;

        return $this;
    }
}
