<?php

namespace App\Entity;

use Core\Core;
use Core\HtmlRenderer;
use Form\FormHandler;
use ORM\Entity\BaseEntity;
use Core\CurrentUser;

class Users extends BaseEntity
{
    protected $firstname;
    protected $lastname;
    protected $email;
    protected $password;
    protected $rawPassword;
    protected $description;
    protected $token;
    protected $status;
    protected $role;
    protected $creation_date;
    protected $update_date;
    protected $username;

    public function __construct()
    {
        parent::__construct();
        $this->setIgnoredFields(['rawPassword', 'creation_date']);
    }

    /**
     * Encrypt a raw password
     *
     * @param string $rawPassword
     * @return string
     */
    public static function encryptPassword(string $rawPassword) : string
    {
        return password_hash($rawPassword, PASSWORD_DEFAULT);
    }

    /**
     * Create the cookie that will allow automatic connection for the user
     *
     * @param Users $current_user
     */
    public function createAutoLoginCookie(Users $current_user) : void
    {
        setcookie('auto_login', base64_encode($current_user->getEmail() . '::' . $current_user->getPassword()), 0, '/');
    }

    /**
     * get logins from the auto_login cookie to fetch the user connected and recreate the session
     *
     * @return bool
     */
    public function fetchCurrentUserSession() : bool
    {
        $logins = explode('::', base64_decode($_COOKIE['auto_login']));

        if (count($logins) == 2) {
            $this->email = $logins[0];
            $this->password = $logins[1];

            $current_user = $this->getOneBy([
                ["email", "=", $this->email]
            ]);

            if ($current_user == false) {
                return false;
            }

            $this->fetchInto($current_user);
            $this->createCurrentUserSession();
            return true;
        }
        return false;
    }

    /**
     * Create the session variable for the logged in user
     *
     * @return void
     */
    public function createCurrentUserSession() : void
    {
        // Keeping the USER_ID to be able to create currentUser
        $_SESSION['USER_ID'] = $this->id;
    }

    /**
     * Return true or false wether the current user is authenticated or not
     *
     * @return bool
     */
    public function checkAuthentication() : bool
    {

        if (isset($_COOKIE['auto_login']) || isset($_SESSION['USER_ID'])) {
            // Login in the user because the auto_login cookie exists
            if (!$this->fetchCurrentUserSession() || $this->status == 0) {
                self::disconnect();
                return false;
            }
            return true;
        }

        return false;
    }

    /**
     * Tries to login the current user and return an array of errors (empty if no errors)
     *
     * @return array
     */
    public function login() : array
    {
        $errors = [];

        // if a user is found, the user object is in $current_user, otherwise $current_user is equal to false
        $current_user = $this->getOneBy([
            ["email", "=", $this->email]
        ]);

        if (!$current_user) {
            // Todo : Use translate module
            $errors[] = 'Identifiants incorrects'; // Email incorrect
        }

        if (is_a($current_user, "App\Entity\Users")) {
            // User found
            if (password_verify($this->getRawPassword(), $current_user->getPassword())) {
                // Correct password, we can login the user if status is 1

                if ($current_user->getStatus() == 0) {
                    $errors[] = "Votre compte est désactivé. Merci de nous contacter.";
                    return $errors;
                }

                // Creating cookie ton maintain connection for the future
                $this->createAutoLoginCookie($current_user);

                // fetching the retrieved user attribute into this user because it is the correct one
                $this->fetchInto($current_user);
                $this->createCurrentUserSession();
                CurrentUser::generateCurrentUser();
            } else {
                // Todo : Use translate module
                $errors[] = 'Identifiants incorrects'; // Mot de passe incorrect
            }
        }

        return $errors;
    }


    /**
     * Disconnect the logged in user
     *
     * @return void
     */
    public static function disconnect() : void
    {
        // setting the auto_login cookie with no value and yesterday date to expire it
        setcookie('auto_login', '', time() - 3600);
        unset($_COOKIE['auto_login']);
        unset($_SESSION['USER_ID']);
        session_destroy();
        CurrentUser::destroyCurrentUser();
    }

    /**
     * Get first name.
     *
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * Set first name
     *
     * @param string $firstname
     * @return Users
     */
    public function setFirstname(string $firstname): Users
    {
        $this->firstname = htmlspecialchars($firstname);

        return $this;
    }

    /**
     * Get last name.
     *
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * Set last name.
     *
     * @param string $lastname
     * @return Users
     */
    public function setLastname(string $lastname): Users
    {
        $this->lastname = htmlspecialchars($lastname);

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string $email
     * @return Users
     */
    public function setEmail(string $email): Users
    {
        $this->email = htmlspecialchars($email);

        return $this;
    }

    /**
     * Get password.
     *
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * Set password.
     *
     * @param string $password
     * @return Users
     */
    public function setPassword(string $password): Users
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get rawPassword.
     *
     * @return string|null
     */
    public function getRawPassword(): ?string
    {
        return $this->rawPassword;
    }

    /**
     * Set rawPassword.
     *
     * @param string $rawPassword
     * @return Users
     */
    public function setRawPassword(string $rawPassword): Users
    {
        $this->rawPassword = $rawPassword;
        $this->password = self::encryptPassword($this->rawPassword);

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     * @return Users
     */
    public function setDescription(string $description): Users
    {
        $this->description = htmlspecialchars($description);

        return $this;
    }

    /**
     * Get token.
     *
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * Generate a random token
     *
     * @throws \Exception
     */
    public function generateToken(): void
    {
        $this->token = bin2hex(random_bytes(10));
    }

    /**
     * Clean the token
     */
    public function cleanToken(): void
    {
        $this->token = '';
    }

    /**
     * Set token
     *
     * @param string $token
     */
    protected function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param int $status
     * @return Users
     */
    public function setStatus(int $status): Users
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get role.
     *
     * @return string|null
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * Set role.
     *
     * @param string $role
     * @return Users
     */
    public function setRole(string $role): Users
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get creation date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getCreationDate(): \DateTime
    {
        return new \DateTime($this->creation_date);
    }

    /**
     * Set creation date.
     *
     * @param \DateTime $creation_date
     * @return Users
     */
    public function setCreationDate(\DateTime $creation_date): Users
    {
        $this->creation_date = $creation_date->getTimestamp();
        return $this;
    }

    /**
     * Get update date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getUpdateDate(): \DateTime
    {
        return new \DateTime($this->update_date);
    }

    /**
     * Set update date.
     *
     * @param \DateTime $update_date
     * @return Users
     */
    public function setUpdateDate(\DateTime $update_date): Users
    {
        $this->update_date = $update_date->getTimestamp();

        return $this;
    }

    /**
     * Get username.
     *
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param string $username
     * @return Users
     */
    public function setUsername(string $username): Users
    {
        $this->username = htmlspecialchars($username);

        return $this;
    }

    /**
     * Get profile img
     *
     * @return string
     */
    public function getProfileImg(): string
    {
        $imgPath = "/assets/images/upload/profile/" . $this->getId() . ".png";

        if (!file_exists($GLOBALS['BASE_DIR'] . "public" . $imgPath)) {
            $imgPath = "/assets/images/default-profile.png";
        }

        return $imgPath;
    }

    /**
     * Replace profile img
     *
     * @param array $file
     * @return bool
     */
    public function replaceProfileImg(array $file): bool
    {
        return FormHandler::uploadFile(
            $file,
            $this->getId() . ".png",
            "images/upload/profile/"
        );
    }
}
