<?php

namespace App\Entity;

use ORM\Entity\BaseEntityCollection;

class ReactionsCollection extends BaseEntityCollection
{
    public function __construct(array $entities = [])
    {
        parent::__construct($entities);
    }
}
