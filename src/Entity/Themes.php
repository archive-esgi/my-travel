<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Themes extends BaseEntity
{
    protected $name;
    protected $slug;
    protected $image_path;
    protected $description;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     * @return Themes
     */
    public function setName(string $name): Themes
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     * @return Themes
     */
    public function setSlug(string $slug): Themes
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get image path.
     *
     * @return string|null
     */
    public function getImagePath(): ?string
    {
        return $this->image_path;
    }

    /**
     * Set image path.
     *
     * @param string $image_path
     * @return Themes
     */
    public function setImagePath(string $image_path): Themes
    {
        $this->image_path = $image_path;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     * @return Themes
     */
    public function setDescription(string $description): Themes
    {
        $this->description = $description;

        return $this;
    }
}
