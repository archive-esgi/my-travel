<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Tags extends BaseEntity
{
    protected $label;
    protected $creation_date;

    public function __construct()
    {
        parent::__construct();
        $this->setIgnoredFields(['creation_date']);
    }

    /**
     * Get label.
     *
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * Set label.
     *
     * @param string $label
     * @return Tags
     */
    public function setLabel(string $label): Tags
    {
        $this->label = htmlspecialchars($label);

        return $this;
    }

    /**
     * Get creation date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getCreationDate(): \DateTime
    {
        return new \DateTime($this->creation_date);
    }

    /**
     * Set creation date.
     *
     * @param \DateTime $creation_date
     * @return Tags
     */
    public function setCreationDate(\DateTime $creation_date): Tags
    {
        $this->creation_date = $creation_date->getTimestamp();
        return $this;
    }
}
