<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Media extends BaseEntity
{
    protected $step_id;
    protected $path;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get step id.
     *
     * @return int|null
     */
    public function getStepId(): ?int
    {
        return $this->step_id;
    }

    /**
     * Set step id.
     *
     * @param int $step_id
     * @return Media
     */
    public function setStepId(int $step_id): Media
    {
        $this->step_id = $step_id;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * Set path.
     *
     * @param string $path
     * @return Media
     */
    public function setPath(string $path): Media
    {
        $this->path = $path;

        return $this;
    }
}
