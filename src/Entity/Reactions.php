<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Reactions extends BaseEntity
{
    protected $user_id;
    protected $trip_id;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get user id.
     *
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    /**
     * Set user id.
     *
     * @param int $user_id
     * @return Reactions
     */
    public function setUserId(int $user_id): Reactions
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get trip id.
     *
     * @return int|null
     */
    public function getTripId(): ?int
    {
        return $this->trip_id;
    }

    /**
     * Set trip id.
     *
     * @param int $trip_id
     * @return Reactions
     */
    public function setTripId(int $trip_id): Reactions
    {
        $this->trip_id = $trip_id;

        return $this;
    }
}
