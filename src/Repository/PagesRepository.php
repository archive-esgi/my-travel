<?php

namespace App\Repository;

use App\Entity\Pages;

use ORM\ORMHandler;

class PagesRepository
{
    /**
     * Get one page by
     *
     * @param array $where
     * @return bool|\ORM\Entity\BaseEntity
     */
    public function getOnePageBy(array $where, $orderBy = null)
    {
        $page = ORMHandler::getOneEntityBy(new Pages(), $where, null, $orderBy);
        return $page;
    }

    /**
     * Create a page
     *
     * @param array $pageData
     */
    public function createPage(array $pageData)
    {
        $page = new Pages();
        $page->setName($pageData['page-name']);
        $page->setTitle($pageData['page-title']);
        $page->setSlug(str_replace(' ', '_', $pageData['page-slug']));
        $page->setContent($pageData['page-content']);
        $page->setLocation($pageData['page-location']);
        $page->save();
    }

    /**
     * Get all pages
     *
     * @return array
     */
    public function getAllPages($orderBy = null): array
    {
        $pages = ORMHandler::getAllEntities(new Pages(), null, $orderBy);
        return $pages->getEntities();
    }

    /**
     * Get all pages by
     *
     * @param array $where
     * @return array
     */
    public function getAllPagesBy(array $where, $orderBy = null): array
    {
        $pages = ORMHandler::getAllEntitiesBy(new Pages(), $where, null, $orderBy);
        return $pages->getEntities();
    }

    /**
     * Get all pages where the search query parameter matches with a page's title
     *
     * @param string $searchQuery
     * @return array
     */
    public function getAllPagesLike($searchQuery): array
    {
        $pages = ORMHandler::getAllEntitiesBy(new Pages(), [
            ["title", " like ", "%". strtolower(htmlspecialchars($searchQuery)) ."%"]
        ]);
        return $pages->getEntities();
    }

    /**
     * Delete a page
     *
     * @param array $where
     * @return boolean
     */
    public function deletePage(array $where): bool
    {
        return ORMHandler::deleteEntities(new Pages(), $where);
    }

    /**
     * Update a page
     *
     * @param array $pageData
     * @return boolean
     */
    public function updatePage(array $pageData)
    {
        $page = new Pages();
        $page->setId($pageData['page-id']);
        $page->fill();
        $page->setName($pageData['page-name']);
        $page->setTitle($pageData['page-title']);
        $page->setSlug($pageData['page-slug']);
        $page->setContent($pageData['page-content']);
        $page->setLocation($pageData['page-location']);
        $page->save();
    }

    /**
     * Get every pages for a location
     *
     * @param string $location
     * @return array
     */
    public function getLocationPages(string $location): array
    {
        return $this->getAllPagesBy([["location", "=", $location]]);
    }
}
