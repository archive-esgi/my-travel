<?php

namespace App\Repository;

use ORM\ORMHandler;
use App\Entity\TripsTags;

class TripsTagsRepository
{

    /**
     * create a tripTag
     *
     */
    public function createTripTag($tripTagData)
    {
        $tripTag = new TripsTags();
        $tripTag->setTripId($tripTagData["tripId"]);
        $tripTag->setTagId($tripTagData["tagId"]);
        $tripTag->save();
    }

    /**
     * Get all tripsTags
     *
     * @return array
     */
    public function getAllTripsTags($orderBy = null): array
    {
        $tags = ORMHandler::getAllEntities(new Tags(), null, $orderBy);
        return $tags->getEntities();
    }

    /**
     * Get all tripsTags by
     *
     * @return array
     */
    public function getAllTripsTagsBy($where): array
    {
        $tripsTags = ORMHandler::getAllEntitiesBy(new TripsTags(), $where);
        return $tripsTags->getEntities();
    }

    /**
     * Delete one or more tripTag
     *
     * @param array $where
     * @return boolean
     */
    public function deleteTripTag(array $where): bool
    {
        return ORMHandler::deleteEntities(new TripsTags(), $where);
    }
}
