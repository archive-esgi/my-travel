<?php

namespace App\Repository;

use ORM\ORMHandler;
use App\Entity\Steps;

class StepsRepository
{
    /**
     * Create a step
     *
     * @param array $stepData
     */
    public function createStep(array $stepData)
    {
        $step = new Steps();
        $step->setTitle($stepData['title']);
        $step->setDescription($stepData['description']);
        $step->setImagePath($stepData['imagePath']);
        $step->setTripId($stepData['tripId']);
        $step->setStartingDate($stepData['startingDate']);
        if ($stepData['endingDate']) {
            $step->setEndingDate($stepData['endingDate']);
        }
        if ($stepData['longitude']) {
            $step->setLongitude($stepData['longitude']);
        }
        if ($stepData['latitude']) {
            $step->setLatitude($stepData['latitude']);
        }
        $step->save();
    }

    /**
     * Get all steps
     *
     * @return array
     */
    public function getAllSteps($orderBy = null): array
    {
        $steps = ORMHandler::getAllEntities(new Steps(), null, $orderBy);
        return $steps->getEntities();
    }

    /**
     * Get all steps by
     *
     * @param array $where
     * @return array
     */
    public function getAllStepsBy(array $where, $orderBy = null): array
    {
        $steps = ORMHandler::getAllEntitiesBy(new Steps(), $where, null, $orderBy);
        return $steps->getEntities();
    }

    /**
     * Get one step by
     *
     * @param array $where
     * @return bool|\ORM\Entity\BaseEntity
     */
    public function getOneStepBy(array $where, $orderBy = null)
    {
        $step = ORMHandler::getOneEntityBy(new Steps(), $where, null, $orderBy);
        return $step;
    }

    /**
     * Update a step
     *
     * @param string $stepData
     */
    public function editStep($stepData)
    {
        $step = $this->getOneStepBy([['id', '=', $stepData['step-id']]]);
        $step->setTitle($stepData['title']);
        $step->setDescription($stepData['description']);
        $step->setImagePath($stepData['image-path']);
        $step->setStartingDate($stepData['starting-date']);
        $step->setEndingDate($stepData['ending-date']);
        if ($stepData['longitude']) {
            $step->setLongitude($stepData['longitude']);
        }
        if ($stepData['latitude']) {
            $step->setLatitude($stepData['latitude']);
        }

        $step->save();
    }

    /**
     * Get all steps where the search query parameter matches with a step's title
     *
     * @param string $searchQuery
     * @return array
     */
    public function getAllStepsLike($searchQuery): array
    {
        $steps = ORMHandler::getAllEntitiesBy(new Steps(), [
            ["title", " like ", "%". strtolower(htmlspecialchars($searchQuery)) ."%"]
        ]);
        return $steps->getEntities();
    }

    /**
     * Delete a step
     *
     * @param array $where
     * @return boolean
     */
    public function deleteStep(array $where): bool
    {
        return ORMHandler::deleteEntities(new Steps(), $where);
    }
}
