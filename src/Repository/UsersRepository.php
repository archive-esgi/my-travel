<?php

namespace App\Repository;

use ORM\ORMHandler;
use App\Entity\Users;

class UsersRepository
{
    /**
     * Get all users
     *
     * @return array
     */
    public function getAllUsers($orderBy = null): array
    {
        $users = ORMHandler::getAllEntities(new Users(), null, $orderBy);
        return $users->getEntities();
    }

    /**
     * Get all users with a search query parameter
     *
     * @param string $searchQuery
     * @return array
     */
    public function getAllUsersLike($searchQuery): array
    {
        $users = ORMHandler::getAllEntitiesBy(new users(), [
            ["lastname", " like ", "%". strtolower(htmlspecialchars($searchQuery)) ."%"]
        ]);
        return $users->getEntities();
    }

    /**
     * Get one user by
     *
     * @param array $where
     * @return Users
     */
    public function getOneUserBy(array $where): Users
    {
        $user = ORMHandler::getOneEntityBy(new Users(), $where);
        return $user;
    }

    /**
     * Delete a user
     *
     * @param array $where
     * @return boolean
     */
    public function deleteUser(array $where): bool
    {
        return ORMHandler::deleteEntities(new Users(), $where);
    }

    /**
     * Update a user
     *
     * @param array $userData
     * @return boolean
     */
    public function updateUser(array $userData)
    {
        $user = new Users();
        $user->setId($userData['id']);
        $user->fill();
        $user->setLastname($userData['lastname']);
        $user->setFirstname($userData['firstname']);
        $user->setUsername($userData['username']);
        $user->setEmail($userData['email']);
        $user->setRole($userData['role']);
        $user->setDescription($userData['description']);
        $user->setStatus($userData['status']);
        $user->save();
    }
}
