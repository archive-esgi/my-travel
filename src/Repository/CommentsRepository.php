<?php

namespace App\Repository;

use ORM\ORMHandler;
use App\Entity\Comments;

class CommentsRepository
{

    /**
     * Post one comment
     *
     */
    public function postComment($commentData)
    {
        $comment = new Comments();
        $comment->setText($commentData["text"]);
        $comment->setUserId($commentData["userId"]);
        $comment->setTripId($commentData["tripId"]);
        $comment->save();
    }

    /**
     * Get all comments
     *
     * @return array
     */
    public function getAllComments($orderBy = null): array
    {
        $comments = ORMHandler::getAllEntities(new Comments(), null, $orderBy);
        return $comments->getEntities();
    }

    /**
     * Get all comments by
     *
     * @return array
     */
    public function getAllCommentsBy($where): array
    {
        $comments = ORMHandler::getAllEntitiesBy(new Comments(), $where);
        return $comments->getEntities();
    }

    /**
     * Delete a comment
     *
     * @param array $where
     * @return boolean
     */
    public function deleteComment(array $where): bool
    {
        return ORMHandler::deleteEntities(new Comments(), $where);
    }

    /**
     * Update a comment
     *
     * @param array $where
     */
    public function updateComment(array $commentData)
    {
        $comment = new Comments();
        $comment->setId($commentData['id']);
        $comment->fill();
        $comment->setText($commentData['text']);
        $comment->save();
    }

    /**
     * Get all comments with a search query parameter
     *
     * @param string $searchQuery
     * @return array
     */
    public function getAllCommentsLike($searchQuery): array
    {
        $comments = ORMHandler::getAllEntitiesBy(new Comments(), [
            ["text", " like ", "%". strtolower(htmlspecialchars($searchQuery)) ."%"]
        ]);
        return $comments->getEntities();
    }
}
