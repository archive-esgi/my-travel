{% import templates.error.error %}

{% block content %}
<main>
    <div class="error-bg-img bg-image-text vh-100 text-center">
        <h2 class="heading-2"><?= Core\Core::translate('error', 'error') ?> 404</h2>
        <h6 class="heading-6"><?= Core\Core::translate('error', 'message.404') ?></h6>
        <?php if ($GLOBALS['ENVIRONMENT'] === "development") : ?>
            <p class="text-subtitle m-t-10"><?php echo $errorMessage; ?></p>
        <?php endif; ?>
        <a href="{% url home %}" class="btn btn-primary m-t-10"><?= Core\Core::translate('error', 'backHome') ?></a>
    </div>
</main>
{% endblock content %}