{% import templates.back.back %}

{% block content %}
<main>
    {% import templates.back.sidenav %}
    <div class="p-l-200 p-t-80">
        <section id="dashboard-card">
            <div class="row">
                <div class="col-3 col-lg-12 col-xxl-6">
                    <div class="card">
                        <i class="material-icons text-primary">person_add</i>
                        <div class="card-body">
                            <p class="text-paragraph"><?= Core\Core::translate('admin.dashboard', 'numberUsers') ?></p>
                            <p class="heading-5"><?= $usersCount ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-3 col-lg-12 col-xxl-6">
                    <div class="card">
                        <i class="material-icons text-success">place</i>
                        <div class="card-body">
                            <p class="text-paragraph"><?= Core\Core::translate('admin.dashboard', 'numberSteps') ?></p>
                            <p class="heading-5"><?= $stepsCount ?> </p>
                        </div>
                    </div>
                </div>
                <div class="col-3 col-lg-12 col-xxl-6">
                    <div class="card">
                        <i class="material-icons text-warning">flight</i>
                        <div class="card-body">
                            <p class="text-paragraph"><?= Core\Core::translate('admin.dashboard', 'numberTrips') ?></p>
                            <p class="heading-5"><?= $tripsCount ?> </p>
                        </div>
                    </div>
                </div>
                <div class="col-3 col-lg-12 col-xxl-6">
                    <div class="card">
                        <i class="material-icons text-grey">comment</i>
                        <div class="card-body">
                            <p class="text-paragraph"><?= Core\Core::translate('admin.dashboard', 'numberComments') ?></p>
                            <p class="heading-5"><?= $commentsCount ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <canvas id="trafic-chart" width="400" height="130"></canvas>
                        <script>
                        const ctx = document.getElementById("trafic-chart").getContext('2d');
                        let week = []
                        for (let i = 0; i < 7; i++) {
                            const dayDate = new Date();
                            dayDate.setDate(dayDate.getDate() - i);
                            dayDate.setHours(0, 0, 0, 0);
                            week[6 - i] = dayDate.toLocaleDateString();
                        }
                        const myChart = new Chart(ctx, {
                            type: 'line',
                            data: {
                                labels: week,
                                datasets: [
                                    {
                                        label: 'Utilisateurs',
                                        data: Object.values(<?= json_encode($usersByDate, true) ?>),
                                        borderColor: '#3e92f9',
                                        fill: false,
                                    },
                                    {
                                        label: 'Étapes',
                                        data: Object.values(<?= json_encode($stepsByDate, true) ?>),
                                        borderColor: '#80a91d',
                                        fill: false,
                                    },
                                    {
                                        label: 'Voyages',
                                        data: Object.values(<?= json_encode($tripsByDate, true) ?>),
                                        borderColor: '#fbf532',
                                        fill: false,
                                    },
                                    {
                                        label: 'Commentaires',
                                        data: Object.values(<?= json_encode($commentsByDate, true) ?>),
                                        borderColor: '#747373',
                                        fill: false,
                                    },

                            ]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: 'Statistiques par jours'
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: false
                                },
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            fixedStepSize: 1
                                        }
                                    }],
                                },
                            }
                        });
                        </script>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
{% endblock content %}