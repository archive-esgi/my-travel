{% import templates.mail %}

{% block title %}
<span><?= $title ?></span>
{% endblock title %}

{% block content %}
<span><?= $body ?></span>
{% endblock content %}

{% block footer %}
<td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#f8f9fb; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#7bb84f">
    <a target="_blank" style="text-decoration:none; color:#f8f9fb; display:block; padding:12px 10px 10px;" href="<?= $completeURL ?>"><?= Core\Core::translate('mail', 'signup.button') ?></a>
</td>
{% endblock footer %}
