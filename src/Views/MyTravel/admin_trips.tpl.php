{% import templates.back.back %}
{% block content %}
<main>
    {% import templates.back.sidenav %}
    <div class="p-l-200 p-t-80">
        <section id="users-list">
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6 m-t-20"><?= Core\Core::translate('admin.trip', 'listTrips'); ?></h6>
                </div>
            </div>
            <?php if ($successMessage) : ?>
                <div class="alert alert-success alert-bottom-right">
                    <p><?= Core\Core::translate('message.success', $successMessage); ?></p>  
                    <span class="close-alert">&times;</span>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="add-user">
                                <form action="{% url back.trips %}" method="POST">
                                    <div class="form-group m-t-5 m-r-20">
                                        <input
                                        class="form-control-default form-control-default-icon text-paragraph search-icon"
                                        placeholder="<?= Core\Core::translate('admin.trip', 'searchTrip'); ?>"
                                        type="text"
                                        name="searchQuery"
                                        id="searchQuery"
                                        value="<?= (isset($searchQuery) ? $searchQuery : null); ?>"
                                        >
                                    </div>
                                </form>
                            </div>
                            <div class="x-scroll">
                                <table class="table table-rounded">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?= Core\Core::translate('admin.trip', 'user'); ?></th>
                                            <th><?= Core\Core::translate('admin.trip', 'title'); ?></th>
                                            <th width="50%"><?= Core\Core::translate('admin.trip', 'description'); ?></th>
                                            <th><?= Core\Core::translate('admin.trip', 'startDate'); ?></th>
                                            <th><?= Core\Core::translate('admin.trip', 'endDate'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'creationDate'); ?></th>
                                            <th><?= Core\Core::translate('admin.trip', 'action'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($trips as $key => $trip): ?>
                                            <?php $user = $usersRepo->getOneUserBy([["id", "=", $trip->getUserId()]]); ?> 
                                            <tr>
                                                <td><?= $trip->getId() ?></td>
                                                <td width="10%"><?= $user->getLastname() . ' ' . $user->getFirstname() . ' (#' . $trip->getUserId() . ')' ?></td>
                                                <td><?= $trip->getTitle() ?></td>
                                                <td><?= $trip->getExcerpt() ?></td>
                                                <td><?= strftime('%d/%m/%Y', $trip->getStartingDate()->getTimestamp()) ?></td>
                                                <td><?= strftime('%d/%m/%Y', $trip->getEndingDate()->getTimestamp()) ?></td>
                                                <td width="10%"><?= strftime('%d/%m/%Y', $trip->getCreationDate()->getTimestamp()) ?></td>
                                                <td width="5%">
                                                    <a class="btn btn-danger btn-icon-only click-to-open" data-modal="<?= 'modal-delete-trip-'.$trip->getId(); ?>"><i class="material-icons">delete</i></a>
                                                    <div id="<?= 'modal-delete-trip-'.$trip->getId(); ?>" class="modal">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <p class="text-subtitle"><?= Core\Core::translate('admin.trip', 'confirmatioDeletion'); ?></p>
                                                                <span class="close close-modal">&times;</span>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p><?= Core\Core::translate('admin.trip', 'sureToDeleteTrip'); ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a class="btn btn-danger cancel"><?= Core\Core::translate('admin.trip', 'non'); ?></a>
                                                                <form method="POST" action="{% url back.trip_delete %}">
                                                                    <input type="hidden" name="trip-id" value="<?= $trip->getId(); ?>" />
                                                                    <button class="btn btn-success" type="submit"><?= Core\Core::translate('admin.trip', 'ok'); ?></a>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
{% endblock content %}