{% import templates.front.front %}

{% block content %}
<main>
    <div class="signin-bg-img bg-image-text vh-100">
        <div>
            <div class="card card-form">
                <div class="card-body">
                    <p class="card-title text-center m-b-15"><?= Core\Core::translate('user.create_trip', 'helpPostTrip') ?></p>
                    <p class="text-paragraph"><?= Core\Core::translate('user.create_trip', 'tripInformation') ?></p>
                    <?php if ($errorMessage): ?>
                        <p class="error-message text-center text-danger m-t-10">
                            <?= Core\Core::translate('message.errors', $errorMessage); ?> 
                        </p>
                    <?php endif; ?>
                    <form action="{% url <?=$createTripForm["action"]?> %}" method="<?=$createTripForm["method"]?>" id="<?=$createTripForm["id"]?>" enctype="multipart/form-data">
                        <?php foreach($createTripForm['fields'] as $fieldName => $field): ?>
                            <div class="form-group">
                                <?php if ($field["type"] != "textarea"): ?>
                                    <input class="<?=$field["class"]?>" placeholder="<?= empty($field['other']['placeholder']) ? '' : Core\Core::translate('user.create_trip', $field['other']['placeholder']); ?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>"
                                           value="<?= $form_params[$field["name"]] ?? '' ?>" id="<?=$field["id"]?>" <?=array_keys($field, "required")[0]?>/>
                                <?php else: ?>
                                    <textarea minlength="150" rows="10" class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>"
                                        <?=array_keys($field, "required")[0]?>><?= $form_params[$field["name"]] ?? '' ?></textarea>
                                <?php endif; ?>
                                <label class="placeholder" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('user.create_trip', $field['other']['label']); ?></label>
                            </div>
                        <?php endforeach; ?>
                    </form>
                    <div class="d-flex-end">
                        <a class="btn btn-danger m-r-10" type="submit" href="{% url trip.search %}"><?= Core\Core::translate('user.create_trip', 'cancel') ?></a>
                        <button class="btn btn-primary show-alert" type="submit" form="<?=$createTripForm["id"]?>" value="Continuer"><?= Core\Core::translate('user.create_trip', 'publish') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
{% endblock content %}