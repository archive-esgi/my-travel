{% import templates.front.front %}

{% block content %}
<main>
    <section>
        <div class="home-bg-img bg-image-text vh-80">
            <h1 class="heading-1"><?= $site_name ?></h1>
            <p class="text-subtitle m-b-20"><?= $site_desc ?></p>
            <a href="{% url user.show_signin_form %}" class="btn btn-primary"><?= Core\Core::translate('home', 'ctaJoinCommunity') ?></a>
        </div>
    </section>
    <div class="container">
        <section>
            <div class="row">
                <div class="col-12">
                    <h5 class="heading-5 m-t-20 m-b-0 text-grey"><?= Core\Core::translate('home', 'latestTrips') ?></h5>
                </div>
            </div>
            <div class="row">
                <?php if (count($lastTrips) > 0) : ?>
                    <?php foreach (array_reverse($lastTrips) as $key => $trip) : ?>
                        <div class="col-4 col-xl-6 col-md-12">
                            <div class="card">
                                <div class="card-img-top" style="background-image: url('<?= $trip->getImagePath() ? $trip->getImagePath() : '/assets/images/img-1.jpeg' ?>');"></div>
                                <div class="card-body">
                                    <h1 class="card-title"><?= $trip->getTitle() ?></h1>
                                    <p class="card-text"><?= $trip->getExcerpt() ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else : ?>
                    <div class="col-12 text-center">
                        <p class="text-subtitle m-t-20"><?= Core\Core::translate('home', 'noTrip') ?></p>
                    </div>
                <?php endif; ?>
                <div id="myModal" class="modal">
                    <div class="modal-content">
                        <div class="modal-header">
                            <p class="text-subtitle"><?= Core\Core::translate('home', 'signUpNow') ?></p>
                            <span class="close close-modal">&times;</span>
                        </div>
                        <div class="modal-body">
                            <p><?= Core\Core::translate('home', 'loggedToContinue') ?></p>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-success"><?= Core\Core::translate('home', 'signUp') ?></a>
                            <a class="btn btn-primary"><?= Core\Core::translate('home', 'signIn') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
{% endblock content %}
{% import templates.front.footer %}