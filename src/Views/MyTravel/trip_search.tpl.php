{% import templates.front.front %}

{% block content %}
<main>
    <section>
        <div class="search-travel-bg-img bg-image-text vh-40">
            <div class="col-12">
                <div class="form-group text-center">
                    <form action="{% url trip.search %}" method="POST" id="trip-search">
                        <input
                        class="form-control-default form-control-default-icon form-control-default-icon-lg text-paragraph search-icon"
                        placeholder="<?= Core\Core::translate('trip_search', 'proposition'); ?>"
                        type="text"
                        name="searchQuery"
                        id="searchQuery"
                        value="<?= (isset($searchQuery) ? $searchQuery : null); ?>"
                        >
                    </form>
                </div>
            </div>
        </div>
    </section>
    <?php if ($successMessage) : ?>
        <div class="alert alert-success alert-bottom-right">
            <p><?= Core\Core::translate('message.success', $successMessage); ?></p>
            <span class="close-alert">&times;</span>
        </div>
    <?php endif; ?>
    <?php if ($welcomeMessage) : ?>
        <div class="alert alert-success alert-bottom-right">
            <p><?= $welcomeMessage ?></p>
            <span class="close-alert">&times;</span>
        </div>
    <?php endif; ?>
    <div class="container <?= count($trips) == 0 ? 'min-height-container' : ''; ?>">
        <section id="last-articles">
            <div class="row">
                <div class="col-12">
                    <h5 class="heading-5 text-center text-grey m-t-20 m-b-0"><?= count($trips).' '.(count($trips) > 1 ? Core\Core::translate('trip_search', 'trips') : Core\Core::translate('trip_search', 'trip')).Core\Core::translate('trip_search', 'discover'); ?></h5>
                </div>
            </div>
            <?php if ($tag): ?>
                <div class="row">
                    <div class="col-12">
                        <p class="text-subtitle m-y-20">
                            <span class="m-t-15"><?= Core\Core::translate('trip_search', 'searchResult'); ?></span>
                            <span class="badge badge-primary badge-pill">
                                <span class="m-r-5"><?= $tag->getLabel(); ?></span>
                                <span><a class="text-white" href="{% url trip.search %}">x</a></span>
                            </span>
                        </p>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <?php if (count($trips) > 0): ?>
                    <?php foreach (array_reverse($trips) as $key => $trip): ?>
                        <div class="col-4 col-xl-6 col-md-12">
                            <div class="card">
                                <div class="card-img-top" style="background-image: url('<?= $trip->getImagePath() ? $trip->getImagePath() : '/assets/images/img-1.jpeg'; ?>');"></div>
                                <div class="card-body">
                                    <h1 class="card-title"><?= $trip->getTitle(); ?></h1>
                                    <p class="card-text"><?= $trip->getExcerpt(); ?></p>
                                    <a href="{% url trip.page %}<?= '?trip_id='.$trip->getId(); ?>" class="btn btn-primary"><?= Core\Core::translate('trip_search', 'seeTrip'); ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="col-12 text-center">
                        <p class="text-subtitle m-t-20"><?= Core\Core::translate('trip_search', 'currentlyNoTrips'); ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    </div>
</main>
{% endblock content %}
{% import templates.front.footer %}