{% import templates.back.back %}
{% block content %}
<main>
    {% import templates.back.sidenav %}
    <div class="p-l-200 p-t-80">
        <section id="users-list">
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6 m-t-20"><?= Core\Core::translate('admin.comment', 'listComment'); ?></h6>
                </div>
            </div>
            <?php if ($successMessage) : ?>
                <div class="alert alert-success alert-bottom-right">
                    <p><?= Core\Core::translate('message.success', $successMessage); ?></p>  
                    <span class="close-alert">&times;</span>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="add-user">
                                <form action="{% url back.comments %}" method="POST">
                                    <div class="form-group m-t-5 m-r-20">
                                        <input
                                        class="form-control-default form-control-default-icon text-paragraph search-icon"
                                        placeholder="<?= Core\Core::translate('admin.comment', 'searchComment'); ?>"
                                        type="text"
                                        name="searchQuery"
                                        id="searchQuery"
                                        value="<?= (isset($searchQuery) ? $searchQuery : null); ?>"
                                        >
                                    </div>
                                </form>
                            </div>
                            <div class="x-scroll">
                                <table class="table table-rounded">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?= Core\Core::translate('admin.comment', 'user'); ?></th>
                                            <th><?= Core\Core::translate('admin.comment', 'text'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'creationDate'); ?></th>
                                            <th><?= Core\Core::translate('admin.comment', 'action'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($comments as $key => $comment): ?>
                                            <?php $user = $usersRepo->getOneUserBy([["id", "=", $comment->getUserId()]]); ?> 
                                            <tr>
                                                <td><?= $comment->getId(); ?></td>
                                                <td><?= $user->getLastname() . ' ' . $user->getFirstname() . ' (#' . $comment->getUserId() . ')' ?></td>
                                                <td width="50%"><?= $comment->getText(); ?></td>
                                                <td><?= strftime('%d/%m/%Y', $comment->getCreationDate()->getTimestamp()); ?></td>
                                                <td>
                                                    <a class="btn btn-danger btn-icon-only click-to-open" data-modal="<?= 'modal-delete-comment-'.$comment->getId(); ?>"><i class="material-icons">delete</i></a>
                                                    <div id="<?= 'modal-delete-comment-'.$comment->getId(); ?>" class="modal">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <p class="text-subtitle"><?= Core\Core::translate('admin.comment', 'confirmatioDeletion'); ?></p>
                                                                <span class="close close-modal">&times;</span>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p><?= Core\Core::translate('admin.comment', 'sureToDeleteComment'); ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a class="btn btn-danger cancel"><?= Core\Core::translate('admin.comment', 'non'); ?></a>
                                                                <form method="POST" action="{% url back.comment_delete %}">
                                                                    <input type="hidden" name="comment-id" value="<?= $comment->getId(); ?>" />
                                                                    <button class="btn btn-success" type="submit"><?= Core\Core::translate('admin.comment', 'ok'); ?></a>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
{% endblock content %}