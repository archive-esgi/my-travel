{% import templates.installer.installer %}

{% block content %}
<main>
    <div id="installer" class="signin-bg-img bg-image-text vh-100">
        <div class="card card-form">
            <div class="card-body">
                <div class="text-center">
                    <img class="logo-medium" src="/assets/images/logos/logo-primary.svg" alt="">
                    <h2 class="m-y-10">MyTravel</h2>
                    <p class="text-subtitle m-b-10"><?= \Core\Core::translate('installer', 'installation') ?></p>
                    <p><?= \Core\Core::translate('installer', 'index.introduction') ?></p>
                    <p class="m-t-10"><?= \Core\Core::translate('installer', 'index.chooseLanguage') ?></p>
                    <?php if ($errorMessage) : ?>
                        <p class="error-message text-center text-danger">
                            <?= $errorMessage ?>
                        </p>
                    <?php endif; ?>
                </div>
                <form action="{% url <?=$indexSetupForm["action"]?> %}" method="<?=$indexSetupForm["method"]?>" id="<?=$indexSetupForm["id"]?>">
                    <?php foreach ($indexSetupForm['fields'] as $fieldName => $field) : ?>
                        <div class="form-group">
                            <select class="<?=$field["class"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>">
                                <?php foreach ($field["options"] as $option) : ?>
                                    <option value="<?= $option['value'] ?>"
                                        <?php if ($option['value'] === $site_language) : ?>
                                            selected
                                        <?php endif; ?>
                                    >
                                        <?= $option['desc'] ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    <?php endforeach; ?>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-rounded btn-primary" form="<?=$indexSetupForm["id"]?>">
                    <?= \Core\Core::translate('installer', 'index.nextButton') ?>
                </button>
            </div>
        </div>
    </div>
</main>
{% endblock content %}