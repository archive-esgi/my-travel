{% import templates.installer.installer %}

{% block content %}
<main>
    <div id="installer" class="finish-setup-bg-img bg-image-text vh-100">
        <div class="card card-form">
            <div class="card-body">
                <div class="text-center">
                    <img class="logo-medium" src="/assets/images/logos/logo-primary.svg" alt="">
                    <h2><?= \Core\Core::translate('installer', 'installation') ?></h2>
                    <p class="text-subtitle m-b-10"><?= \Core\Core::translate('installer', 'finish.title') ?></p>
                    <p><?= \Core\Core::translate('installer', 'finish.introduction') ?></p>
                    <?php if ($errorMessage) : ?>
                        <p class="error-message text-center text-danger m-t-10">
                            <?= $errorMessage ?>
                        </p>
                    <?php endif; ?>
                </div>
                <form action="{% url <?=$finishSetupForm["action"]?> %}" method="<?=$finishSetupForm["method"]?>" id="<?=$finishSetupForm["id"]?>">
                    <?php foreach ($finishSetupForm['fields'] as $fieldName => $field) : ?>
                        <?php if ($field['type'] == 'select') : ?>
                            <div class="form-group">
                                <label for="<?=$field["name"]?>"><?= \Core\Core::translate('installer.finish_setup', $field["other"]["label"]) ?></label>
                                <select class="<?=$field["class"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>">
                                    <?php foreach ($field["options"] as $option) : ?>
                                        <option value="<?= $option['value'] ?>"
                                            <?php if ($option['value'] === \Core\Core::getConfig('signup.sendMail')) : ?>
                                                selected
                                            <?php endif; ?>
                                        >
                                            <?= $option['desc'] ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        <?php else : ?>
                            <div class="form-group">
                                <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" value="<?= $form_params[$field["name"]] ?? '' ?>" id="<?=$field["id"]?>" <?=array_keys($field, "required")[0]?>/>
                                <label class="placeholder placeholder-icon" for="<?=$field["name"]?>"><?= \Core\Core::translate('installer.finish_setup', $field["other"]["label"]) ?></label>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-rounded btn-primary" form="<?=$finishSetupForm["id"]?>"><?= \Core\Core::translate('installer', 'finish.nextButton') ?></button>
            </div>
        </div>
    </div>
</main>
{% endblock content %}