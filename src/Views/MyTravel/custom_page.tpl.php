{% import templates.front.front %}

{% block content %}
<main class="m-b-200">
    <section id="trip-steps">
        <div class="search-travel-bg-img bg-image-text vh-40"></div>
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h1 class="heading-4 trip-title"><?= $pageTitle ?></h1>
                </div>
                <div class="divider col-5"></div>
                <div class="col-12">
                    <p><?= $pageContent ?></p>
                </div>
            </div>
        </div>
    </section>
</main>
{% endblock content %}
{% import templates.front.footer %}