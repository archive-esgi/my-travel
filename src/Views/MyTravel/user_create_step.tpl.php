{% import templates.front.front %}

{% block content %}
<main>
    <div class="signin-bg-img bg-image-text vh-100">
        <div>
            <div class="card card-form">
                <div class="card-body">
                    <p class="card-title text-center m-b-15"><?= Core\Core::translate('user.create_step', 'enrichStory') ?></p>
                    <p class="text-paragraph"><?= Core\Core::translate('user.create_step', 'informationStep') ?></p>
                    <?php if ($errorMessage): ?>
                        <p class="error-message text-center text-danger m-t-10">
                            <?= Core\Core::translate('message.errors', $errorMessage); ?>
                        </p>
                    <?php endif; ?>
                    <form action="{% url <?=$createStepForm["action"]?> %}" method="<?=$createStepForm["method"]?>" id="<?=$createStepForm["id"]?>" enctype="multipart/form-data">
                        <?php foreach($createStepForm['fields'] as $fieldName => $field): ?>
                            <div class="form-group">
                                <?php if ($field["type"] != "textarea"): ?>
                                    <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" value="<?= $form_params[$field["name"]] ?? '' ?>" id="<?=$field["id"]?>" <?=array_keys($field, "required")[0]?>/>
                                <?php else: ?>
                                    <textarea minlength="150" rows="10" class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" <?=array_keys($field, "required")[0]?>><?= $form_params[$field["name"]] ?? '' ?></textarea>
                                <?php endif; ?>
                                <label class="placeholder" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('user.create_step', $field['other']['label']); ?></label>
                            </div>
                        <?php endforeach; ?>

                        <span><?= Core\Core::translate('user.create_step', 'shareTraveledArea') ?></span>
                        <div class="m-y-20" style="height: 300px;" id="step-position-map"></div>

                        <input id="trip-id" name="trip-id" type="hidden" value="<?= $tripId ?>">
                        <input id="step-lng" name="step-lng" type="hidden" value="">
                        <input id="step-lat" name="step-lat" type="hidden" value="">
                    </form>
                    <div class="d-flex-end">
                        <a class="btn btn-danger m-r-10" href="{% url user.show_edit_trip_form %}<?= '?trip_id=' . $tripId ?>"><?= Core\Core::translate('user.create_step', 'cancel') ?></a>
                        <button class="btn btn-primary show-alert" type="submit" form="<?=$createStepForm["id"]?>" value="Publier"><?= Core\Core::translate('user.create_step', 'publish') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
{% endblock content %}