{% import templates.front.front %}

{% block content %}
<main>
    <section id="cover">
        <div class="search-travel-bg-img bg-image-text vh-30">
            <img class="logo-small" src="/assets/images/logos/logo-secondary.svg" alt="Logo">
        </div>
    </section>
    <div class="container">
        <section id="trip-info">
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="d-flex flex-wrap m-t-20">
                        <h6 class="heading-6 text-grey"><?= Core\Core::translate('user.edit_trip', 'tripInformation') ?></h6>
                        <div>
                            <a class="btn btn-primary m-x-15" href="{% url user.show_create_step_form %}<?= '?trip_id=' . $trip->getId() ?>"><i class="material-icons">add</i><?= Core\Core::translate('user.edit_trip', 'addStep') ?></a>
                            <a class="btn btn-primary" href="{% url trip.page %}<?= '?trip_id=' . $trip->getId() ?>"><i class="material-icons">remove_red_eye</i> <?= Core\Core::translate('user.edit_trip', 'overview') ?></a>
                        </div>
                    </div>

                    <?php if ($errorMessage): ?>
                        <div class="alert alert-error alert-bottom-right">
                            <p><?= Core\Core::translate('message.errors', $errorMessage); ?></p>
                            <span class="close-alert">&times;</span>
                        </div>
                    <?php endif; ?>
                    <?php if ($successMessage): ?>
                        <div class="alert alert-success alert-bottom-right">
                            <p><?= Core\Core::translate('message.success', $successMessage); ?></p>     
                            <span class="close-alert">&times;</span>
                        </div>
                    <?php endif; ?>

                    <form id="edit-trip" name="edit-trip" action="{% url user.edit_trip %}" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <input class="form-control w-100" type="text" name="trip-title" id="trip-title" value="<?= $trip->getTitle() ?>" required>
                            <label class="placeholder" for="trip-title"><?= Core\Core::translate('user.edit_trip', 'title') ?></label>
                        </div>

                        <div class="form-group">
                            <textarea minlength="150" class="form-control w-100" name="trip-description" id="trip-description" rows="10" required><?= $trip->getDescription() ?></textarea>
                            <label class="placeholder" for="trip-description"><?= Core\Core::translate('user.edit_trip', 'description') ?></label>
                        </div>

                        <div>
                            <img id="trip-image-preview" class="card" src="<?= $trip->getImagePath() ?>" alt="">
                            <div class="m-t-10">
                                <label class="btn btn btn-success" for="trip-image">
                                    <span><i class="material-icons m-r-5">photo_camera</i><?= Core\Core::translate('user.edit_trip', 'editPhoto') ?></span>
                                </label>
                                <input data-preview="trip-image-preview" class="type-file d-none" type="file" name="trip-image" id="trip-image">
                            </div>
                        </div>

                        <div class="form-group">
                            <input class="form-control w-100" type="date" name="trip-starting-date" id="trip-starting-date" value="<?= $trip->getStartingDate() ? strftime("%Y-%m-%d", $trip->getStartingDate()->getTimestamp()) : '' ?>">
                            <label class="placeholder" for="trip-starting-date"><?= Core\Core::translate('user.edit_trip', 'startDate') ?></label>
                        </div>

                        <div class="form-group">
                            <input class="form-control w-100" type="date" name="trip-ending-date" id="trip-ending-date" value="<?= $trip->getEndingDate() ? strftime("%Y-%m-%d", $trip->getEndingDate()->getTimestamp()) : '' ?>">
                            <label class="placeholder" for="trip-ending-date"><?= Core\Core::translate('user.edit_trip', 'endDate') ?></label>
                        </div>

                        <div class="form-group">
                            <input class="form-control w-100" type="text" name="trip-tags" id="trip-tags" placeholder="exemple: France; montagne; plage; ..." value="<?= $tagsLabelsImploded ? $tagsLabelsImploded : '' ?>">
                            <label class="placeholder" for="trip-tags"><?= Core\Core::translate('user.edit_trip', 'enterTag') ?></label>
                        </div>

                        <input id="trip-id" name="trip-id" type="hidden" value="<?= $trip->getId() ?>">
                    </form>
                    <button class="btn btn-primary m-t-10 m-b-20" type="submit" form="edit-trip" value="Enregistrer"><i class="material-icons m-r-5">save</i><?= Core\Core::translate('user.edit_trip', 'save') ?></button>
                </div>
            </div>
        </section>
        <section id="steps-info">
            <?php foreach($steps as $key => $step): ?>
                <div id="<?= 'modal-delete-step-'.$step->getId(); ?>" class="modal">
                    <div class="modal-content">
                        <div class="modal-header">
                            <p class="text-subtitle"><?= Core\Core::translate('user.edit_trip', 'confirmDeletion') ?></p>
                            <span class="close close-modal">&times;</span>
                        </div>
                        <div class="modal-body">
                            <p><?= Core\Core::translate('user.edit_trip', 'sureToDeleteTrip') ?></p>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-danger cancel"><?= Core\Core::translate('user.edit_trip', 'non') ?></a>
                            <form method="POST" action="{% url user.delete_step %}">
                                <input type="hidden" name="step-id" value="<?= $step->getId(); ?>" />
                                <input type="hidden" name="trip-id" value="<?= $trip->getId(); ?>" />
                                <button class="btn btn-success" type="submit"><?= Core\Core::translate('user.edit_trip', 'ok') ?></a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8 offset-2">
                        <div class="d-flex flex-wrap m-t-30">
                            <h6 class="heading-6 text-grey m-r-20"><?= Core\Core::translate('user.edit_trip', 'step') ?> <?= $key + 1 . ' / ' . count($steps) ?></h6>
                            <a class="btn btn-danger click-to-open" data-modal="<?= 'modal-delete-step-'.$step->getId(); ?>"><i class="material-icons">delete</i> <?= Core\Core::translate('user.edit_trip', 'deleteStep') ?></a>
                        </div>
                        <form id="edit-step-<?= $step->getId() ?>" name="edit-step-<?= $step->getId() ?>" action="{% url user.edit_step %}" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <input class="form-control w-100 " type="text" name="title" value="<?= $step->getTitle() ?>" required>
                                <label class="placeholder" for="title"><?= Core\Core::translate('user.edit_trip', 'title') ?></label>
                            </div>
                            <div class="form-group">
                                <textarea minlength="150" class="form-control w-100" name="description" rows="10" required><?= $step->getDescription() ?></textarea>
                                <label class="placeholder" for="description"><?= Core\Core::translate('user.edit_trip', 'description') ?></label>
                            </div>
                            <div>
                                <img id="step-image-<?= $step->getId() ?>-preview" class="card" src="<?= $step->getImagePath() ?>" alt="">
                                <div>
                                    <label class="btn btn btn-success" for="step-image-<?= $step->getId() ?>">
                                        <span><i class="material-icons m-r-5">photo_camera</i><?= Core\Core::translate('user.edit_trip', 'editPhoto') ?></span>
                                    </label>
                                    <input data-preview="step-image-<?= $step->getId() ?>-preview" class="type-file d-none" type="file" name="image" id="step-image-<?= $step->getId() ?>">
                                </div>
                                <!--  -->
                                
                            </div>
                            <div class="form-group">
                                <input value="<?= strftime("%Y-%m-%d", $step->getStartingDate()->getTimestamp()) ?>" class="form-control w-100" type="date" name="starting-date" required>
                                <label class="placeholder transformed-label" for="starting-date"><?= Core\Core::translate('user.edit_trip', 'startDate') ?></label>
                            </div>
                            <div class="form-group">
                                <input value="<?= $step->getEndingDate() != null ? strftime("%Y-%m-%d", $step->getEndingDate()->getTimestamp()) : '' ?>" class="form-control w-100" type="date" name="ending-date">
                                <label class="placeholder transformed-label" for="ending-date"><?= Core\Core::translate('user.edit_trip', 'endDate') ?></label>
                            </div>

                            <p><?= Core\Core::translate('user.edit_trip', 'zoned') ?></p>

                            <div style="height: 300px;" class="card" id="<?= 'step-position-map-' . $step->getId() ?>"></div>
                            <script>
                                document.addEventListener('DOMContentLoaded', () => {
                                    if (document.getElementById('step-position-map-' + <?= $step->getId() ?>)) {

                                        <?php if ($step->getLongitude()): ?>
                                            let stepPositionMap = L.map('step-position-map-' + <?= $step->getId() ?> ).setView([<?= $step->getLatitude() ?>, <?= $step->getLongitude() ?>], 8);
                                        <?php else: ?>
                                            let stepPositionMap = L.map('step-position-map-' + <?= $step->getId() ?> ).setView([48.8534, 2.3488], 5);
                                        <?php endif; ?>

                                        if (stepPositionMap) {
                                            L.tileLayer('https://api.tiles.mapbox.com/v4/mapbox.run-bike-hike/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFsZWtvcyIsImEiOiJjancyMWtuYTgwMmJqNDhxbjJseTd2YmlpIn0.lIqOI3DJRryq2498wCYdtw', {
                                                maxZoom: 18,
                                                id: 'mapbox.streets',
                                                accessToken: 'pk.eyJ1IjoibWFsZWtvcyIsImEiOiJjancyMWtuYTgwMmJqNDhxbjJseTd2YmlpIn0.lIqOI3DJRryq2498wCYdtw'
                                            }).addTo(stepPositionMap);

                                            <?php if ($step->getLongitude()): ?>
                                                let initialSavedMarker = new L.marker([<?= $step->getLatitude() ?>, <?= $step->getLongitude() ?>]);
                                                let initialSavedMarkerRemoved = false;
                                                stepPositionMap.addLayer(initialSavedMarker);

                                                document.getElementById("step-lat-<?= $step->getId() ?>").value = <?= $step->getLatitude() ?>;
                                                document.getElementById("step-lng-<?= $step->getId() ?>").value = <?= $step->getLongitude() ?>;
                                            <?php endif; ?>

                                            let marker = L.marker();

                                            stepPositionMap.on('click', (e) => {
                                                <?php if ($step->getLongitude()): ?>
                                                    if (!initialSavedMarkerRemoved) {
                                                        stepPositionMap.removeLayer(initialSavedMarker);
                                                        initialSavedMarkerRemoved = true;
                                                    }
                                                <?php endif; ?>

                                                marker.setLatLng(e.latlng).addTo(stepPositionMap);
                                                let lat = e.latlng.lat;
                                                let lng = e.latlng.lng;
                                                document.getElementById("step-lat-<?= $step->getId() ?>").value = lat;
                                                document.getElementById("step-lng-<?= $step->getId() ?>").value = lng;
                                            });
                                        }
                                    }
                                });
                            </script>

                            <input name="trip-id" type="hidden" value="<?= $trip->getId() ?>">
                            <input name="step-id" type="hidden" value="<?= $step->getId() ?>">
                            <input name="position" type="hidden" value="<?= $key + 1 ?>">
                            <input id="step-lng-<?= $step->getId() ?>" name="longitude" type="hidden" value="">
                            <input id="step-lat-<?= $step->getId() ?>" name="latitude" type="hidden" value="">
                            <button class="btn btn-primary m-y-10" type="submit" form="edit-step-<?= $step->getId() ?>" value="Enregistrer"><i class="material-icons m-r-5">save</i><?= Core\Core::translate('user.edit_trip', 'save') ?></button>
                        </form>
                    </div>
                </div>
            <?php endforeach; ?>
        </section>
    </div>
</main>
{% import templates.front.footer %}
{% endblock content %}