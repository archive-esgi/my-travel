{% import templates.front.front %}

{% block content %}
<main>
    <div id="signin" class="signup-bg-img bg-image-text vh-100">
        <div>
            <div class="card card-form">
                <div class="card-body">
                    <div class="text-center">
                        <img class="logo-medium" src="/assets/images/logos/logo-primary.svg" alt="">
                        <p class="text-subtitle"><?= Core\Core::translate('signup.signup', 'inscription') ?></p>
                    </div>
                    <?php if ($successMessage): ?>
                        <p class="m-t-10 success-message text-center text-success">
                            <?= $successMessage ?>
                        </p>
                    <?php endif; ?>
                    <?php if ($errorMessage): ?>
                        <p class="m-t-10 error-message text-center text-danger">
                            <?= $errorMessage ?>
                        </p>
                    <?php endif; ?>
                    <form action="{% url <?=$signupForm["action"]?> %}" method="<?=$signupForm["method"]?>" id="<?=$signupForm["id"]?>">
                        <?php foreach($signupForm['fields'] as $fieldName => $field): ?>
                            <div class="<?=implode(' ', $field["other"]["inputIconClass"])?>">
                                <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>"
                                       value="<?= $form_params[$field["name"]] ?? '' ?>" <?=array_keys($field, "required")[0]?>/>
                                <label class="placeholder" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('signup.signup', $field['other']['label']); ?></label>
                            </div>
                        <?php endforeach; ?>
                    </form>
                    <button class="btn btn-rounded btn-primary w-100" type="submit" form="signupform"><?= Core\Core::translate('signup.signup', 'signup') ?></button>
                </div>
            </div>
        </div>
    </div>
</main>
{% endblock content %}