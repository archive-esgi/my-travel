{% import templates.front.front %}
{% block content %}
<main>
    <section id="cover">
        <div class="search-travel-bg-img bg-image-text vh-30">
            <img class="logo-small" src="/assets/images/logos/logo-secondary.svg" alt="Logo">
        </div>
    </section>
    <section id="user-settings">
        <div class="container min-height-container">
            <div class="row">
                <div class="col-2 offset-2 col-xl-8 offset-xl-2">
                    <div class="card">
                        <nav>
                            <ul>
                                <li><a href="{% url user.settings.general %}" class="nav-setting"><?= Core\Core::translate('user.settings_general', 'personalInformations') ?></a></li>
                                <li><a href="{% url user.settings.password %}" class="nav-setting"><?= Core\Core::translate('user.settings_general', 'password') ?></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-6 col-xl-8 offset-xl-2">
                    <div>
                        <div class="card">
                            <div class="card-body">
                                <h6 class="heading-6"><?= Core\Core::translate('user.settings_password', 'changePassword') ?></h6>
                                <div class="divider m-y-16"></div>
                                
                                <?php if ($successMessage) : ?>
                                    <div class="alert alert-success alert-bottom-right">
                                        <p><?= Core\Core::translate('user.settings_password', $successMessage); ?></p>
                                        <span class="close-alert">&times;</span>
                                    </div>
                                <?php endif; ?>
                                <?php if ($errorMessage) : ?>
                                    <div class="alert alert-error alert-bottom-right">
                                        <p><?= Core\Core::translate('user.settings_password', $errorMessage); ?></p>
                                        <span class="close-alert">&times;</span>
                                    </div>
                                <?php endif; ?>

                                <form action="{% url <?= $settingsPasswordForm["action"]?> %}" method="<?= $settingsPasswordForm["method"]?>" id="<?= $settingsPasswordForm["id"]?>">
                                    <?php foreach ($settingsPasswordForm['fields'] as $fieldName => $field) : ?>
                                        <div class="form-group">
                                            <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" <?=array_keys($field, "required")[0]?>/>
                                            <label class="placeholder" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('user.settings_password', $field['other']['label']); ?></label>
                                        </div>
                                    <?php endforeach; ?>
                                </form>
                                <button class="btn btn-primary" type="submit" form="<?= $settingsPasswordForm["id"]?>" value="Enregistrer"><?= Core\Core::translate('user.settings_password', 'save') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
{% import templates.front.footer %}
{% endblock content %}