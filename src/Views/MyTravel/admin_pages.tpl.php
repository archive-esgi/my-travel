{% import templates.back.back %}

{% block content %}
<main>
    {% import templates.back.sidenav %}
    <div class="p-l-200 p-t-80">
        <section id="pages-list">
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6 m-t-20"><?= Core\Core::translate('admin.page', 'listPages'); ?></h6>
                </div>
            </div>
            <?php if ($successMessage) : ?>
                <div class="alert alert-success alert-bottom-right">
                    <p><?= $successMessage ?></p>
                    <span class="close-alert">&times;</span>
                </div>
            <?php endif; ?>
            <?php if ($errorMessage) : ?>
                <div class="alert alert-error alert-bottom-right">
                    <p><?= $errorMessage ?></p>
                    <span class="close-alert">&times;</span>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="add-page">
                                <form action="{% url back.pages %}" method="POST">
                                    <div class="form-group m-t-5 m-r-20">
                                        <input
                                        class="form-control-default form-control-default-icon text-paragraph search-icon"
                                        placeholder="Rechercher page"
                                        type="text"
                                        name="searchQuery"
                                        id="searchQuery"
                                        value="<?= (isset($searchQuery) ? $searchQuery : null); ?>"
                                        >
                                    </div>
                                </form>
                                <a class="btn btn-primary click-to-open" data-modal="modal-add-page"><i class="material-icons">add</i><?= Core\Core::translate('admin.page', 'add'); ?></a>
                            </div>
                            <div id="modal-add-page" class="modal">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <p class="text-subtitle"><?= Core\Core::translate('admin.page', 'addingPage'); ?></p>
                                        <span class="close close-modal">&times;</span>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{% url back.add_page %}" method="POST" id="add-page-form" name="add-page-form">
                                            <div class="form-group">
                                                <input class="form-control w-100" type="text" name="page-name" required>
                                                <label class="placeholder" for="page-name"><?= Core\Core::translate('admin.page', 'name'); ?></label>
                                            </div>

                                            <div class="form-group">
                                                <input class="form-control w-100" type="text" name="page-title" required>
                                                <label class="placeholder" for="page-title"><?= Core\Core::translate('admin.page', 'title'); ?></label>
                                            </div>

                                            <div class="form-group">
                                                <input class="form-control w-100" type="text" name="page-slug" required placeholder="ex: page_de_mon_site">
                                                <label class="placeholder" for="page-slug"><?= Core\Core::translate('admin.page', 'identifiant'); ?></label>
                                            </div>

                                            <div class="form-group m-t-50">
                                                <textarea class="form-control w-100 tiny-mce" name="page-content"></textarea>
                                            </div>

                                            <div class="form-group m-t-20">
                                                <label><?= Core\Core::translate('admin.page', 'location'); ?></label>
                                                <select id="page-location" name="page-location">
                                                    <option selected value="header"><?= Core\Core::translate('admin.page', 'header'); ?></option>
                                                    <option value="footer"><?= Core\Core::translate('admin.page', 'footer'); ?></option>
                                                </select>
                                            </div>
                                        </form>                                
                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-danger cancel"><?= Core\Core::translate('admin.page', 'cancel'); ?></a>
                                        <button class="btn btn-success" type="submit" form="add-page-form"><?= Core\Core::translate('admin.page', 'validate'); ?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="x-scroll">
                                <table class="table table-rounded">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?= Core\Core::translate('admin.page', 'name'); ?></th>
                                            <th><?= Core\Core::translate('admin.page', 'title'); ?></th>
                                            <th><?= Core\Core::translate('admin.page', 'identifiantShort'); ?></th>
                                            <th><?= Core\Core::translate('admin.page', 'locationShort'); ?></th>
                                            <th><?= Core\Core::translate('admin.page', 'dateAdded'); ?></th>
                                            <th><?= Core\Core::translate('admin.page', 'actions'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($pages as $key => $page): ?>
                                            <tr>
                                                <td><?= $page->getId() ?></td>
                                                <td><?= $page->getName() ?></td>
                                                <td><?= $page->getTitle() ?></td>
                                                <td><?= $page->getSlug() ?></td>
                                                <td><?= $page->getLocation() === 'header' ?  Core\Core::translate('admin.page', 'header') : Core\Core::translate('admin.page', 'footer'); ?></td>
                                                <td><?= strftime('%d/%m/%Y', $page->getCreationDate()->getTimestamp()) ?></td>
                                                <td>
                                                    <a class="btn btn-success btn-icon-only click-to-open" data-modal="<?= 'modal-edit-page-'.$page->getId(); ?>"><i class="material-icons">edit</i></a>
                                                    <a class="btn btn-danger btn-icon-only click-to-open" data-modal="<?= 'modal-delete-page-'.$page->getId(); ?>"><i class="material-icons">delete</i></a>
                                                    <div id="<?= 'modal-edit-page-'.$page->getId(); ?>" class="modal">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <p class="text-subtitle"><?= Core\Core::translate('admin.page', 'editPage'); ?></p>
                                                                <span class="close close-modal">&times;</span>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{% url back.page_update %}" method="POST" id="edit-page-form-<?= $page->getId() ?>">
                                                                    <div class="form-group">
                                                                        <input class="form-control w-100" type="text" name="page-name" value="<?= $page->getName() ?>" required>
                                                                        <label class="placeholder" for="page-name"><?= Core\Core::translate('admin.page', 'name'); ?></label>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <input class="form-control w-100" type="text" name="page-title" value="<?= $page->getTitle() ?>" required>
                                                                        <label class="placeholder" for="page-title"><?= Core\Core::translate('admin.page', 'title'); ?></label>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <input class="form-control w-100" type="text" name="page-slug" value="<?= $page->getSlug() ?>" required placeholder="ex: page_de_mon_site">
                                                                        <label class="placeholder" for="page-slug"><?= Core\Core::translate('admin.page', 'identifiant'); ?></label>
                                                                    </div>

                                                                    <div class="form-group m-t-50">
                                                                        <textarea class="form-control w-100 tiny-mce" name="page-content"><?= htmlspecialchars($page->getContent()) ?></textarea>
                                                                    </div>

                                                                    <div class="form-group m-t-20">
                                                                        <label><?= Core\Core::translate('admin.page', 'location'); ?></label>
                                                                        <select id="page-location" name="page-location">
                                                                            <option <?= $page->getLocation() == 'header' ? 'selected' : '' ?> value="header"><?= Core\Core::translate('admin.page', 'header'); ?></option>
                                                                            <option <?= $page->getLocation() == 'footer' ? 'selected' : '' ?> value="footer"><?= Core\Core::translate('admin.page', 'footer'); ?></option>
                                                                        </select>
                                                                    </div>
                                                                    <input type="hidden" name="old-page-slug" value="<?= $page->getSlug(); ?>" />
                                                                    <input type="hidden" name="old-page-location" value="<?= $page->getLocation(); ?>" />
                                                                    <input type="hidden" name="page-id" value="<?= $page->getId(); ?>" />
                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a class="btn btn-danger cancel"><?= Core\Core::translate('admin.page', 'cancel'); ?></a>
                                                                <button class="btn btn-success" type="submit" form="edit-page-form-<?= $page->getId() ?>"><?= Core\Core::translate('admin.page', 'validate'); ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="<?= 'modal-delete-page-'.$page->getId(); ?>" class="modal">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <p class="text-subtitle"><?= Core\Core::translate('admin.page', 'confirmDelete'); ?></p>
                                                                <span class="close close-modal">&times;</span>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p><?= Core\Core::translate('admin.page', 'confirmDeleteMessage'); ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a class="btn btn-danger cancel"><?= Core\Core::translate('admin.page', 'buttonNo'); ?></a>
                                                                <form method="POST" action="{% url back.page_delete %}">
                                                                    <input type="hidden" name="page-id" value="<?= $page->getId(); ?>" />
                                                                    <button class="btn btn-success" type="submit"><?= Core\Core::translate('admin.page', 'buttonYes'); ?></a>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
{% endblock content %}