<header>
    <nav class="navbar fixed-top bg-primary navbar-admin">
        <a href="#" class="navbar-brand"><?= $site_name ?> - <?= Core\Core::translate('template.back', 'administration') ?></a>
        <div class="navbar-right d-flex-end">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <div class="dropdown">
                        <div class="m-r-24">
                            <img class="profile-picture rounded" src="<?= Core\Core::getCurrentUser()->getProfileImg() ?>" alt="">
                        </div>
                        <div class="dropdown-content">
                            <a href="{% url trip.search %}" class="dropdown-item">
                                <i class="material-icons m-r-5">airplanemode_active</i>
                                <?= $site_name ?>
                            </a>
                            <a href="{% url user.disconnect %}" class="dropdown-item">
                                <i class="material-icons m-r-5">exit_to_app</i>
                                <?= Core\Core::translate('template.back', 'disconnect') ?>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>