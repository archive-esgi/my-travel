<footer>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="divider m-y-40"></div>
            </div>
            <div class="col-3 col-lg-6 col-sm-12">
                <img class="logo-large" src="/assets/images/logos/logo-primary.svg" alt="Logo">
            </div>
            <?php if (count($pagesRepo->getLocationPages('footer')) > 0): ?>
                <div class="col-3 col-lg-6 col-sm-12">
                    <h5 class="text-subtitle text-uppercase"><?= Core\Core::translate('footer', 'other') ?></h5>
                    <ul>
                        <?php foreach ($pagesRepo->getLocationPages('footer') as $page): ?>
                            <li class="footer-item"><a class="footer-link" href="/page?slug=<?= $page->getSlug() ?>"><?= $page->getName() ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <?php if (!empty(Core\Core::getConfig('site.footer.social_networks.facebook')) ||
                !empty(Core\Core::getConfig('site.footer.social_networks.twitter')) ||
                !empty(Core\Core::getConfig('site.footer.social_networks.instagram'))
            ) : ?>
            <div class="col-3 col-lg-6 col-sm-12">
                <h5 class="text-subtitle text-uppercase"><?= Core\Core::translate('footer', 'findUs') ?></h5>
                <ul>
                    <?php if (!empty(Core\Core::getConfig('site.footer.social_networks.facebook'))) : ?>
                        <li class="footer-item">
                            <a class="footer-link" target="_blank" href="<?= Core\Core::getConfig('site.footer.social_networks.facebook') ?>">Facebook</a>
                        </li>
                    <?php endif; ?>

                    <?php if (!empty(Core\Core::getConfig('site.footer.social_networks.instagram'))) : ?>
                        <li class="footer-item">
                            <a class="footer-link" target="_blank" href="<?= Core\Core::getConfig('site.footer.social_networks.instagram') ?>">Instagram</a>
                        </li>
                    <?php endif; ?>
                    <?php if (!empty(Core\Core::getConfig('site.footer.social_networks.twitter'))) : ?>
                        <li class="footer-item">
                            <a class="footer-link" target="_blank" href="<?= Core\Core::getConfig('site.footer.social_networks.twitter') ?>">Twitter</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <?php endif; ?>

            <?php if (!empty(Core\Core::getConfig('site.footer.contact.mail')) ||
                    !empty(Core\Core::getConfig('site.footer.contact.phone'))
            ) : ?>
                <div class="col-3 col-lg-6 col-sm-12">
                    <h5 class="text-subtitle text-uppercase"><?= Core\Core::translate('footer', 'contactUs') ?></h5>
                    <ul>
                        <?php if (!empty(Core\Core::getConfig('site.footer.contact.mail'))) : ?>
                            <li class="footer-item">
                                <a class="footer-link" href="mailto:<?= Core\Core::getConfig('site.footer.contact.mail') ?>">
                                    <?= Core\Core::getConfig('site.footer.contact.mail') ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (!empty(Core\Core::getConfig('site.footer.contact.phone'))) : ?>
                            <li class="footer-item">
                                <a class="footer-link" href="tel:<?= Core\Core::getConfig('site.footer.contact.phone') ?>">
                                    <?= Core\Core::getConfig('site.footer.contact.phone') ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="footer-copyright">
        <p class="text-subtitle text-center"><?= Core\Core::translate('header', 'rightsReserved') ?> &copy; <?php echo date('Y');?></p>
    </div>
</footer>
