<header>
    <nav class="navbar fixed-top bg-transparent">
        <a href="{% url home %}" class="navbar-brand navbar-fo-brand"><?= $site_name ?></a>
        <div class="navbar-left">
            <ul class="navbar-nav">
                <?php if (\Core\Core::getCurrentUser()->checkAuthentication()) : ?>
                    <li class="nav-item">
                        <a href="{% url trip.search %}" class="nav-link"><?= Core\Core::translate('header', 'trips') ?></a>
                    </li>
                    <?php foreach ($pagesRepo->getLocationPages('header') as $page): ?>
                        <li class="nav-item">
                            <a href="/page?slug=<?= $page->getSlug() ?>" class="nav-link"><?= $page->getName() ?></a>
                        </li>
                    <?php endforeach ?>
                <?php else : ?>
                    <li class="nav-item">
                        <a href="{% url home %}" class="nav-link"><?= Core\Core::translate('header', 'home') ?></a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="navbar-right d-flex-end">
            <ul class="navbar-nav">
                <?php if (\Core\Core::getCurrentUser()->checkAuthentication()) : ?>
                    <li class="nav-item">
                        <div class="dropdown">
                            <a class="nav-link"><?= Core\Core::translate('header', 'publish') ?><i class="material-icons">arrow_drop_down</i></a>
                            <div class="dropdown-content">
                                <a href="{% url user.show_create_trip_form %}" class="dropdown-item"><?= Core\Core::translate('header', 'publishTrip') ?></a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="{% url user.trips %}" class="btn btn-primary nav-link"><?= Core\Core::translate('header', 'myTrips') ?></a>
                    </li>
                    <li class="nav-item">
                        <div class="dropdown">
                            <div class="m-r-24">
                                <img class="profile-picture rounded m-l-20" src="<?= Core\Core::getCurrentUser()->getProfileImg() ?>" alt="">
                            </div>
                            <div class="dropdown-content">
                                <a href="{% url user.settings.general %}" class="dropdown-item">
                                    <i class="material-icons m-r-5">account_circle</i>
                                    <?= Core\Core::translate('header', 'myProfile') ?>
                                </a>
                                <?php if(\Core\Core::getCurrentUser()->getRole() == 'admin'): ?>
                                    <a href="{% url back.home %}" class="dropdown-item">
                                        <i class="material-icons m-r-5">build</i>
                                        <?= Core\Core::translate('header', 'administration') ?>
                                    </a>
                                <?php endif; ?>
                                <a href="{% url user.disconnect %}" class="dropdown-item">
                                    <i class="material-icons m-r-5">exit_to_app</i>
                                    <?= Core\Core::translate('header', 'signOut') ?>
                                </a>
                            </div>
                        </div>
                    </li>
                <?php else : ?>
                    <li class="nav-item">
                        <a href="{% url user.show_signup_form %}" class="nav-link"><?= Core\Core::translate('header', 'signUp') ?></a>
                    </li>
                    <li class="nav-item">
                        <a href="{% url user.show_signin_form %}" class="nav-link"><?= Core\Core::translate('header', 'signIn') ?></a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        
    </nav>
</header>