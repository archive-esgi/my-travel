<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $site_name ?></title>
        <meta name=‘viewport’ content=‘width=device-width, initial-scale=1.0, user-scalable=« no"’>
        <link rel="stylesheet" type="text/css" href="/assets/<?= $theme_name ?>/style/css/main.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="icon" href="/assets/images/icons/logo-icon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
        integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
        crossorigin=""/>
        <script src="/assets/<?= $theme_name ?>/js/navbar.js"></script>
        <script src="/assets/<?= $theme_name ?>/js/modal.js"></script>
        <script src="/assets/<?= $theme_name ?>/js/alert.js"></script>
        <script src="/assets/<?= $theme_name ?>/js/input.js"></script>
        <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=<?= $tinymce_key ?>"></script>
        <script>tinymce.init({ menubar:false, mode:"specific_textareas", editor_selector:'tiny-mce'});</script>
        <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
        integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
        crossorigin=""></script>
        <script src="/assets/<?= $theme_name ?>/js/map.js"></script>
    </head>
    <body>
        {% import templates.front.header %}
        {{ block content }}
    </body>
</html>