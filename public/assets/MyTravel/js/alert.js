document.addEventListener('DOMContentLoaded', () => {
    closeAlert();
});

const closeAlert = () => {
    const closeAlertButtons = document.getElementsByClassName('close-alert');

    for (let i = 0; i < closeAlertButtons.length; i++) {
        setTimeout(() => {
            const alertWrapper = closeAlertButtons[i].parentElement;
            alertWrapper.style.opacity = '0';
        }, 6000);

        closeAlertButtons[i].addEventListener('click', function() {
            const alertWrapper = this.parentElement;
            alertWrapper.style.opacity = '0';
        }
    )}
}
