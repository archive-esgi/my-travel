document.addEventListener('DOMContentLoaded', () => {
    transformInputLabel();
    transformTextareaLabel();
    transformLabelWithPlaceholder();
    inputFileImagePreview();
    transformInputWhenNotRequired();
    transformEmailInputType();
});

const transformInputLabel = () => {
    const inputs = document.querySelectorAll('input[type=date], input[type=file]');

    for (let i = 0; i < inputs.length; i++) {
        if (inputs[i].nextElementSibling) {
            inputs[i].nextElementSibling.classList.add('transformed-label');
        }
        if (inputs[i].type === 'file' && inputs[i].classList.contains('type-file')) {
            if (inputs[i].nextElementSibling) {
                inputs[i].nextElementSibling.classList.remove('transformed-label');
            }
        }
    }
}

const transformTextareaLabel = () => {
    const textareas = document.querySelectorAll('textarea');

    for (let i = 0; i < textareas.length; i++) {
        textareas[i].addEventListener('keyup', function(e) {
            if (e.target.value.length > 0) {
                textareas[i].nextElementSibling.classList.add('transformed-label');
            }
        });
    }
}

const transformLabelWithPlaceholder = () => {
    const inputs = document.getElementsByTagName('input');

    for (let i = 0; i < inputs.length; i++) {
        if (inputs[i].placeholder) {
            if (inputs[i].nextElementSibling) {
                inputs[i].nextElementSibling.classList.add('transformed-label');
            }
        }
    }
}

const inputFileImagePreview = () => {
    const inputsTypeFile = document.getElementsByClassName('type-file');

    for (let i = 0; i < inputsTypeFile.length; i++) {
        inputsTypeFile[i].addEventListener('change', function() {
            const file = this.files[0];
            const reader = new FileReader();
            const preview = document.getElementById(this.dataset.preview);

            reader.onload = function() {
                preview.src = this.result;
            },

            reader.readAsDataURL(file);
        }, false);
    }
}

const transformInputWhenNotRequired = () => {
    const inputs = document.querySelectorAll('input[type=text], input[type=email]');

    for (let i = 0; i < inputs.length; i++) {
        if (inputs[i].required === false && inputs[i].value.length === 0) {
            if (!inputs[i].placeholder) {
                if (inputs[i].nextElementSibling) {
                    inputs[i].nextElementSibling.classList.add('untransformed-label');
                    inputs[i].classList.add('initial-border');
                }
            }
        }

        inputs[i].addEventListener('focus', function() {
            if (inputs[i].nextElementSibling) {
                inputs[i].nextElementSibling.classList.remove('untransformed-label');
                inputs[i].classList.remove('initial-border');
            }
        })

        inputs[i].addEventListener('focusout', function(e) {
            if (e.target.value.length > 0) {
                if (inputs[i].nextElementSibling) {
                    inputs[i].nextElementSibling.classList.remove('untransformed-label');
                    inputs[i].classList.remove('initial-border');
                }
            }

            if (e.target.value.length === 0) {
                if (!inputs[i].placeholder) {
                    if (inputs[i].nextElementSibling) {
                        inputs[i].nextElementSibling.classList.add('untransformed-label');
                        inputs[i].classList.add('initial-border');
                    }
                }
            }
        });
    }
}

const transformEmailInputType = () => {
    const emailInputs = document.querySelectorAll('input[type=email]');

    for (let i = 0; i < emailInputs.length; i++) {
        emailInputs[i].addEventListener('change', function(e) {
            if (e.target.value.length > 0) {
                if (emailInputs[i].nextElementSibling) {
                    emailInputs[i].nextElementSibling.classList.add('transformed-label');
                }
            }
        });
    }
}
