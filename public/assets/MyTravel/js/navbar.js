document.addEventListener('DOMContentLoaded', () => {
    const nav = document.querySelector('nav');
    nav.style.transition = "all 0.6s";

    window.addEventListener('scroll', () => {
        const windowPosition = window.scrollY;

        if (windowPosition > 0) {
            nav.classList.add('navbar-sticky');
        } else {
            nav.classList.remove('navbar-sticky');
        }
    });
});