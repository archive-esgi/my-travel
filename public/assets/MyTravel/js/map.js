document.addEventListener('DOMContentLoaded', () => {
    if (document.getElementById('step-position-map')) {
        let stepPositionMap = L.map('step-position-map').setView([48.8534, 2.3488], 5);
        // pk.eyJ1IjoibWFsZWtvcyIsImEiOiJjancyMWtuYTgwMmJqNDhxbjJseTd2YmlpIn0.lIqOI3DJRryq2498wCYdtw

        L.tileLayer('https://api.tiles.mapbox.com/v4/mapbox.run-bike-hike/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFsZWtvcyIsImEiOiJjancyMWtuYTgwMmJqNDhxbjJseTd2YmlpIn0.lIqOI3DJRryq2498wCYdtw', {
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoibWFsZWtvcyIsImEiOiJjancyMWtuYTgwMmJqNDhxbjJseTd2YmlpIn0.lIqOI3DJRryq2498wCYdtw'
        }).addTo(stepPositionMap);

        marker = L.marker();
        stepPositionMap.on('click', (e) => {
            marker.setLatLng(e.latlng).addTo(stepPositionMap);
            let lat = e.latlng.lat;
            let lng = e.latlng.lng;
            document.getElementById("step-lat").value = lat;
            document.getElementById("step-lng").value = lng;
        });
    }
});