<?php

namespace Router\Exception;

use Router\Entity\ViewResponse;

class ForbiddenAccessException extends \Exception
{
    /**
     * ForbiddenAccessException destructor.
     */
    public function __destruct()
    {
        $message = $this->message . " On " . $this->file . " line " . $this->line . ".";
        new ViewResponse('errors.403', ['errorMessage' => $message], 403);
        exit();
    }
}
