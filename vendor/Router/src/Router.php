<?php

namespace Router;

use App\Entity\Users;
use Core\CurrentUser;
use Core\Core;
use Logger\Logger;
use Logger\LogType;
use Router\Entity\Request;
use Router\Entity\Route;
use Router\Exception\ForbiddenAccessException;
use Router\Exception\HttpNotFoundException;

abstract class Router
{

    /**
     * Get current request.
     *
     * @return Request
     */
    public static function getRequest(): Request
    {
        return new Request();
    }

    /**
     * Redirect the user to a different route using uri or route name
     *
     * @param string $nameOrUri
     * @param int $statusCode
     *
     * @return void
     */
    public static function redirect(string $nameOrUri, int $statusCode = 302): void
    {
        http_response_code($statusCode);
        if (array_key_exists($nameOrUri, self::getRoutes())) {
            header("Location: " . self::getRouteByName($nameOrUri)->getUrlPath());
            return;
        }
        header("Location: " . $nameOrUri);
    }

    /**
     * Return true if the current user can access the requested route or return false otherwise
     *
     * @param Route $route
     *
     * @return boolean
     */
    public static function isCurrentUserAuthorized(Route $route): bool
    {
        $userToCheckAuthentication = Core::getCurrentUser() ? Core::getCurrentUser() : new Users();
        $loggedRouteOption = $route->getOptions()['access']['logged'];
        $allowedRolesRouteOption = $route->getOptions()['access']['roles'];

        if (isset($loggedRouteOption) &&
            (!$userToCheckAuthentication->checkAuthentication() && $loggedRouteOption === true ||
            $userToCheckAuthentication->checkAuthentication() && $loggedRouteOption === false)) {
            return false;
        }

        if (isset($allowedRolesRouteOption) &&
            !in_array($userToCheckAuthentication->getRole(), $allowedRolesRouteOption)) {
            return false;
        }

        return true;
    }

    /**
     * Redirect user on forbidden error
     *
     * @param Route $route
     */
    public static function redirectUnauthorizedUser(Route $route): void
    {
        $redirectRoute = $route->getOptions()['access']['redirect'];

        try {
            if (!empty($redirectRoute)) {
                Router::redirect($redirectRoute);
                return;
            }

            throw new ForbiddenAccessException('Access denied !');
        } catch (ForbiddenAccessException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Get route by URI.
     *
     * @param string $uri
     * @return Route
     */
    public static function getRouteByURI(string $uri)
    {
        try {
            $routes = self::getRoutes();

            foreach ($routes as $name => $route) {
                if ($route['path'] === $uri) {
                    return self::getRouteByName($name);
                }
            }

            throw new HttpNotFoundException("$uri route uri not found.");
        } catch (HttpNotFoundException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Get route by route name.
     *
     * @param string $name
     * @return Route
     */
    public static function getRouteByName(string $name): Route
    {
        try {
            $routes = self::getRoutes();
            if (array_key_exists($name, $routes)) {
                $route = $routes[$name];
                if (array_key_exists('path', $route)) {
                    if (array_key_exists('class', $route)) {
                        if (array_key_exists('action', $route)) {
                            if ((array_key_exists('method', $route)
                                && $route['method'] === $_SERVER['REQUEST_METHOD'])
                                || !array_key_exists('method', $route)) {
                                return new Route(
                                    $name,
                                    $route['path'],
                                    $route['class'],
                                    $route['action'],
                                    $route['options']
                                );
                            }
                        }
                    }
                }
            }

            throw new HttpNotFoundException("$name route name not found.");
        } catch (HttpNotFoundException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Get route by route name for render.
     *
     * @param string $name
     * @param string $method
     * @return Route
     */
    public static function getRouteByNameForRender(string $name): Route
    {
        try {
            $routes = self::getRoutes();
            if (array_key_exists($name, $routes)) {
                $route = $routes[$name];
                if (array_key_exists('path', $route)) {
                    if (array_key_exists('class', $route)) {
                        if (array_key_exists('action', $route)) {
                            return new Route(
                                $name,
                                $route['path'],
                                $route['class'],
                                $route['action'],
                                $route['options']
                            );
                        }
                    }
                }
            }

            throw new HttpNotFoundException("$name route name not found.");
        } catch (HttpNotFoundException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Get route by request.
     *
     * @param Request $request
     * @return Route
     */
    public static function getRouteByRequest(Request $request): Route
    {
        return self::getRouteByURI($request->getUri());
    }

    /**
     * Get all routes on routing configuration.
     *
     * @return array
     */
    private static function getRoutes(): array
    {
        return yaml_parse_file($GLOBALS['BASE_DIR'] . "config/router/routing.yml");
    }
}
