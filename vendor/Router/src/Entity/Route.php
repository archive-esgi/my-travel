<?php

namespace Router\Entity;

class Route
{
    private $name;
    private $urlPath;
    private $class;
    private $action;
    private $options;

    /**
     * Route constructor.
     *
     * @param $name
     * @param $urlPath
     * @param $class
     * @param $action
     * @param array|null $options
     */
    public function __construct(string $name, string $urlPath, string $class, string $action, $options)
    {
        $this->name = $name;
        $this->urlPath = $urlPath;
        $this->class = $class;
        $this->action = $action;
        $this->options = $options;
    }


    /**
     * Get route name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get url path.
     *
     * @return string
     */
    public function getUrlPath(): string
    {
        return $this->urlPath;
    }

    /**
     * Get Controller class.
     *
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Get controller action.
     *
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * Get route options.
     *
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }
}
