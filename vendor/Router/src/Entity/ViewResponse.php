<?php

namespace Router\Entity;

use App\Repository\PagesRepository;
use Core\HtmlRenderer;

class ViewResponse extends Response
{
    private $renderer;

    /**
     * ViewResponse constructor.
     *
     * @param string $view
     * @param array $data
     * @param int $statusCode
     * @param array $headers
     */
    public function __construct(string $view, array $data = [], int $statusCode = null, array $headers = [])
    {
        parent::__construct($statusCode ?? 200, $headers);
        $this->renderer = new HtmlRenderer($view, $data);
        $this->add('pagesRepo', new PagesRepository());
    }

    /**
     * Add var to view(shortcut).
     *
     * @param string $key
     * @param $value
     */
    public function add(string $key, $value)
    {
        $this->renderer->add($key, $value);
    }

    /**
     * ViewResponse destructor.
     */
    public function __destruct()
    {
        parent::__destruct();
        echo $this->renderer->render();
    }
}
