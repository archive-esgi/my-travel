<?php

namespace Router\Entity;

use Router\Router;

class Response
{

    protected $headers;
    protected $statusCode;
    protected $request;

    /**
     * Response constructor.
     *
     * @param int $statusCode
     * @param array $headers
     */
    public function __construct(int $statusCode = null, array $headers = [])
    {
        $this->statusCode = $statusCode ?? 200;
        $this->headers = $headers;
        $this->request = Router::getRequest();
    }

    /**
     * Get response header's.
     *
     * @return array
     */
    protected function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Set response header's.
     *
     * @param array $headers
     * @return Response
     */
    protected function setHeaders(array $headers): Response
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Get response status code.
     *
     * @return int
     */
    protected function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Set response status code.
     *
     * @param int $statusCode
     * @return Response
     */
    protected function setStatusCode(int $statusCode): Response
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Response destructor.
     */
    public function __destruct()
    {
        http_response_code($this->statusCode ?? 200);
    }
}
