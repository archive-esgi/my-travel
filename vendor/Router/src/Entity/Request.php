<?php

namespace Router\Entity;

class Request
{

    private $method;
    private $statusCode;
    private $uri;
    private $request;
    private $query;
    private $cookies;
    private $files;
    private $clientIp;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->statusCode = http_response_code();
        $this->uri = strtok($_SERVER["REQUEST_URI"], '?');
        $this->request = $_POST;
        $this->query = $_GET;
        $this->cookies = $_COOKIE;
        $this->files = $_FILES;
        $this->clientIp = $this->getClientIpFromRequest();
    }

    /**
     * Get request HTTP method
     *
     * @return string|null
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Get request HTTP status code
     *
     * @return string|null
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Get request HTTP parameters (only from POST method)
     *
     * @return array|null
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Get request HTTP parameters (only from GET method)
     *
     * @return array|null
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Get request HTTP cookies
     *
     * @return array|null
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * Get request HTTP uri
     *
     * @return string|null
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return array|null
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Get client IP
     *
     * @return string
     */
    public function getClientIp(): string
    {
        return $this->clientIp;
    }

    /**
     * Get client IP from the web browser request
     * @return string
     */
    private function getClientIpFromRequest(): string
    {
        if (getenv('HTTP_CLIENT_IP')) {
            return getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            return getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            return getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            return getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            return getenv('HTTP_FORWARDED');
        } elseif (getenv('REMOTE_ADDR')) {
            return getenv('REMOTE_ADDR');
        } else {
            return 'unknown';
        }
    }
}
