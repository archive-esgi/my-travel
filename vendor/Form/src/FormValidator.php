<?php

namespace Form;

use Form\Entity\Form;
use Form\Entity\FormField;

abstract class FormValidator
{

    /**
     * Check if the form & data are valid.
     *
     * @param string $formName
     * @param array $data
     * @return array
     */
    public static function validateForm(string $formName, array $data): array
    {
        $formState = [
            'status' => true,
            'errorMessage' => '',
        ];

        $form = FormHandler::getForm($formName);

        if (self::isXSSAttack($form, $data)) {
            $formState = [
                'status' => false,
                'errorMessage' => 'Oups ! Something was wrong ;)',
            ];
        }

        /** @var FormField $field */
        foreach ($form->getFields() as $field) {
            $formState = self::validateFormField($field, $data);

            if (!$formState['status']) {
                break;
            }
        }

        return $formState;
    }

    /**
     * Check if the field & data are valid.
     *
     * @param FormField $formField
     * @param array $data
     * @return array
     */
    public static function validateFormField(FormField $formField, array $data): array
    {
        $fieldState = self::fieldIsRequired($formField, $data);
        if ($fieldState['status']) {
            foreach ($formField->getRules() as $ruleName => $rule) {
                $methodName = $ruleName . "Rule";

                if (method_exists($formField, $methodName)) {
                    $ruleResult = $formField->$methodName($data);

                    if (!$ruleResult['status']) {
                        $fieldState['status'] = false;
                        $fieldState['errorMessage'] = $ruleResult['errorMessage'];
                        break;
                    }
                }
            }
        }

        return $fieldState;
    }

    /**
     * Check if field is required.
     *
     * @param FormField $formField
     * @param array $data
     * @return array
     */
    private static function fieldIsRequired(FormField $formField, array $data): array
    {
        $fieldState = [
            'status' => true,
            'errorMessage' => '',
        ];

        if ($formField->isRequired() && array_key_exists($formField->getName(), $data)) {
            $state['status'] = false;
            $state['errorMessage'] = $formField->getRules()['required'] ?? 'The ' .
                $formField->getName() . ' field is required';
        }

        return $fieldState;
    }

    /**
     * Check if is a XSS Attack.
     *
     * @param Form $form
     * @param array $data
     * @return bool
     */
    private static function isXSSAttack(Form $form, array $data): bool
    {
        return count($form->getFields()) === count($data) ? false : true;
    }
}
