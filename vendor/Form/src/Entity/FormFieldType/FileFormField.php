<?php

namespace Form\Entity\FormFieldType;

use Form\Entity\FormField;

class FileFormField extends FormField
{
    public function __construct(string $id, string $name)
    {
        parent::__construct($id, $name);
        $this->type = 'file';
    }

    public function formatter(array $fieldArray): void
    {
        parent::formatter($fieldArray);
    }

    public function render(): array
    {
        return parent::render();
    }

    /**
     * Check file type.
     *
     * @param array $data
     * @return array
     */
    public function allowTypeRule(array $data): array
    {
        $fileType = strtolower(pathinfo($data[$this->getName()]['name'], PATHINFO_EXTENSION));
        $allowedTypes = $this->getRules()['maxLength']['value'];

        $state = [
            'status' => true,
            'errorMessage' => '',
        ];

        if (isset($allowedTypes) && in_array($fileType, $allowedTypes)) {
            $state = [
                'status' => false,
                'errorMessage' => $this->getRules()['checkType']['errorMessage'] ??
                    'Bad file extension on ' . $this->name . ' field.',
            ];
        }

        return $state;
    }

    /**
     * Check file size.
     *
     * @param array $data
     * @return array
     */
    public function maxSizeRule(array $data): array
    {
        $fileSize = $this->getRules()['maxLength']['value'];

        $state = [
            'status' => true,
            'errorMessage' => '',
        ];

        if (isset($fileSize) && $data[$this->getName()]['size'] > $fileSize) {
            $state = [
                'status' => false,
                'errorMessage' => $this->getRules()['checkType']['errorMessage'] ??
                    'File must be lower than ' . $fileSize . ' on ' . $this->name . ' field.',
            ];
        }

        return $state;
    }
}
