<?php

namespace Form\Entity\FormFieldType;

use Core\Core;
use Form\Entity\FormField;
use Logger\Logger;
use Logger\LogType;
use Router\Exception\ServerErrorException;

class EmailFormField extends FormField
{
    public function __construct(string $id, string $name)
    {
        parent::__construct($id, $name);
        $this->type = 'email';
    }

    public function formatter(array $fieldArray): void
    {
        parent::formatter($fieldArray);
    }

    public function render(): array
    {
        return parent::render();
    }

    /**
     * Check min length of the data.
     *
     * @param array $data
     * @return array
     */
    public function minLengthRule(array $data): array
    {
        $state = [
            'status' => false,
            'errorMessage' => 'MinLength rule must have a value attribute on ' . $this->name . ' field.',
        ];

        try {
            if (isset($this->getRules()['minLength']['value']) && is_int($this->getRules()['minLength']['value'])) {
                if ($this->getRules()['minLength']['value'] < mb_strlen($data[$this->getName()])) {
                    $state = [
                        'status' => true,
                        'errorMessage' => '',
                    ];
                } else {
                    $state = [
                        'status' => false,
                        'errorMessage' => $this->getRules()['minLength']['errorMessage'] ??
                            'Too few characters on ' . $this->name . ' field.',
                    ];
                }
            } else {
                throw new ServerErrorException('Bad value attribute on minLength rule ' .
                    $this->name . ' field. Must be an integer.');
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }

        return $state;
    }

    /**
     * Check max length of the data.
     *
     * @param array $data
     * @return array
     */
    public function maxLengthRule(array $data): array
    {
        $state = [
            'status' => false,
            'errorMessage' => 'MaxLength rule must have a value attribute on ' . $this->name . ' field.',
        ];

        try {
            if (isset($this->getRules()['maxLength']['value']) && is_int($this->getRules()['maxLength']['value'])) {
                if ($this->getRules()['maxLength']['value'] > mb_strlen($data[$this->getName()])) {
                    $state = [
                        'status' => true,
                        'errorMessage' => '',
                    ];
                } else {
                    $state = [
                        'status' => false,
                        'errorMessage' => $this->getRules()['maxLength']['errorMessage'] ??
                            'Too much characters on ' . $this->name . ' field.',
                    ];
                }
            } else {
                throw new ServerErrorException('Bad value attribute on maxLength rule ' .
                    $this->name . ' field. Must be an integer.');
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }

        return $state;
    }

    /**
     * Check lower case of the data.
     *
     * @param array $data
     * @return array
     */
    public function lowerCaseRule(array $data): array
    {

        if (ctype_lower($data[$this->getName()])) {
            $state = [
                'status' => true,
                'errorMessage' => '',
            ];
        } else {
            $state = [
                'status' => false,
                'errorMessage' => $this->getRules()['lowerCase']['errorMessage'] ??
                    'The characters must be on lower case on ' . $this->name . ' field.',
            ];
        }

        return $state;
    }

    /**
     * Check upper case of the data.
     *
     * @param array $data
     * @return array
     */
    public function upperCaseRule(array $data): array
    {

        if (ctype_upper($data[$this->getName()])) {
            $state = [
                'status' => true,
                'errorMessage' => '',
            ];
        } else {
            $state = [
                'status' => false,
                'errorMessage' => $this->getRules()['upperCase']['errorMessage'] ??
                    'The characters must be on upper case on ' . $this->name . ' field.',
            ];
        }

        return $state;
    }
}
