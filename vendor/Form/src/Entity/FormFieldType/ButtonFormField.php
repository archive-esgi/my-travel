<?php

namespace Form\Entity\FormFieldType;

use Form\Entity\FormField;

class ButtonFormField extends FormField
{
    public function __construct(string $id, string $name)
    {
        parent::__construct($id, $name);
        $this->type = 'button';
    }

    public function formatter(array $fieldArray): void
    {
        parent::formatter($fieldArray);
    }

    public function render(): array
    {
        return parent::render();
    }
}
