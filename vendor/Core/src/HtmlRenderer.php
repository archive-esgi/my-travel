<?php

namespace Core;

use Logger\Logger;
use Router\Exception\ServerErrorException;
use Router\Router;

class HtmlRenderer
{
    private $view;
    private $viewName;
    private $data;
    private $themeName;

    public function __construct(string $view, array $data = [])
    {
        $this->themeName = Core::getConfig('theme.name') ?? 'MyTravel';
        $this->viewName = $view;
        $this->setView($view);
        $this->data = $data;
    }

    /**
     * Set view path.
     *
     * @param string $view
     */
    private function setView(string $view): void
    {
        $viewPath = $GLOBALS['BASE_DIR'] .
            "src/Views/" . $this->themeName . "/" .
            str_replace('.', '/', $view) . ".tpl.php";
        try {
            if (file_exists($viewPath)) {
                $this->view = $viewPath;
            } else {
                throw new ServerErrorException($viewPath . " view not found.");
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Add var to view.
     *
     * @param string $key
     * @param $value
     */
    public function add(string $key, $value)
    {
        $forbiddenKeys = [
            'content',
            'match',
            'matches',
            'params',
            'request',
            'theme_name',
            'recaptcha_site_key',
            'tinymce_key',
            'view_name',
            'site_name',
            'site_desc',
        ];

        try {
            if (!array_key_exists($key, $forbiddenKeys)) {
                $this->data[$key] = $value;
            } else {
                throw new ServerErrorException($key .' is a forbidden key name.');
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Make views renderer.
     *
     * @return string
     */
    public function render(): string
    {
        $this->addBasicData();
        $content = $this->formatImports();
        $content = $this->formatBlocks($content);
        $content = $this->formatUrls($content);

        return $content;
    }

    /**
     *  Add basic data to the response.
     */
    private function addBasicData():void
    {
        $this->add('request', $this->request);
        $this->add('theme_name', Core::getConfig('theme.name'));
        $this->add('recaptcha_site_key', Core::getConfig('recaptcha.site_key'));
        $this->add('tinymce_key', Core::getConfig('tinymce.key'));
        $this->add('view_name', $this->viewName);
        $this->add('site_name', Core::getConfig('site.name'));
        $this->add('site_desc', Core::getConfig('site.description'));
        $this->add('site_language', Core::getConfig('site.language'));
    }

    /**
     * Import others views on content.
     *
     * @param string|null $content
     *
     * @return string
     */
    private function formatImports(string $content = null): string
    {
        // Extract var to use on templates.
        extract($this->data);

        // Get origin template render.
        if (empty($content)) {
            ob_start();
            include $this->view;
            $content = ob_get_contents();
            ob_end_clean();
        }

        // Import other templates.
        preg_match_all("#{%\s*import\s+[\w\.]+\s*%}#i", $content, $matches);
        foreach ($matches[0] as $match) {
            $params = explode(" ", trim($match, " \t\n\r\0\x0B{}%"));
            ob_start();
            include($GLOBALS['BASE_DIR'] . "src/Views/" .
                $this->themeName . "/" . str_replace('.', '/', $params[1]) . ".tpl.php");
            $content = str_replace($match, ob_get_clean(), $content);
            ob_end_clean();
        }

        // Recursive import.
        if (count($matches[0]) > 0) {
            $content = $this->formatImports($content);
        }

        return $content;
    }

    /**
     * Return with block formatted.
     *
     * @param string $content
     *
     * @return string
     */
    private function formatBlocks(string $content): string
    {
        preg_match_all("#\{%\s*block\s+(?<name>\w+)\s*%\}\s.*\s\{%\s*endblock\s+\g{name}\s*%\}#is", $content, $matches);
        foreach ($matches[0] as $match) {
            $params = explode(" ", trim($match, " \t\n\r\0\x0B{}%"), 3);
            $block = preg_replace("#{%\s*block\s+" . $params[1] . "\s*%}#i", "", $match);
            $block = preg_replace("#{%\s*endblock\s+" . $params[1] . "\s*%}#i", "", $block);
            $content = str_replace($match, "", $content);

            preg_match_all("#{{\s*block\s+" . $params[1] . "\s*}}#i", $content, $blockuses);
            foreach ($blockuses[0] as $blockUse) {
                $content = str_replace($blockUse, $block, $content);
            }
        }

        return $content;
    }

    /**
     * Return content with the formatted URL's.
     *
     * @param string $content
     *
     * @return string
     */
    private function formatUrls(string $content): string
    {
        preg_match_all("#{%\s*url\s+[\w\.]+\s*%}#i", $content, $matches);
        foreach ($matches[0] as $match) {
            $params = explode(" ", trim($match, " \t\n\r\0\x0B{}%"));
            $url = Router::getRouteByNameForRender($params[1])->getUrlPath();
            $content = str_replace($match, $url, $content);
        }

        return $content;
    }
}
