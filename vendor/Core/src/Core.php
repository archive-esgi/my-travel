<?php

namespace Core;

use Router\Exception\ServerErrorException;
use ORM\ORMHandler;
use App\Entity\Users;

abstract class Core
{
    use ORMHandler;

    /**
     * Get config file path.
     *
     * @param string|null $module
     * @param string|null $filePath
     * @return string
     */
    public static function getConfigFilePath(string $module = null, string $filePath = null): string
    {
        if (!empty($module) && $module !== "app") {
            return $GLOBALS['BASE_DIR'] . 'config/' .
                strtolower($module) .
                "/" .
                str_replace('.', '/', $filePath) . ".yml";
        }

        return $GLOBALS['BASE_DIR'] . "env.yml";
    }

    /**
     * Get configuration from YAML file.
     *
     * @param string $key
     * @param string|NULL $module
     * @param string|NULL $filePath
     *
     * @return array|mixed
     */
    public static function getConfig(string $key, string $module = null, string $filePath = null)
    {
        $configFileExistErrorMessage = "env.yml file doesn't exist.";
        $configKeyExistErrorMessage = $key . " configuration doesn't exist on env.yml.";

        if (!empty($module) && $module !== "app") {
            $configFileExistErrorMessage = "$module/$filePath.yml configuration file doesn't exist.";
            $configKeyExistErrorMessage = $key . " configuration doesn't exist on $module/$filePath.yml.";
        }

        try {
            $configFilePath = self::getConfigFilePath($module, $filePath);
            if (file_exists($configFilePath)) {
                $config = yaml_parse_file($configFilePath);

                $result = self::keyGetValue($key, $config);

                if (!is_null($result)) {
                    return $result;
                } else {
                    if ($module == 'translation' && Core::getConfig('environment') === "production") {
                        return $key;
                    }
                    throw new ServerErrorException($configKeyExistErrorMessage);
                }
            } else {
                throw new ServerErrorException($configFileExistErrorMessage);
            }
        } catch (ServerErrorException $e) {
            self::logger($e->getMessage(), LogType::ERROR);
        }
    }

    /**
     * Set configuration from YAML file.
     *
     * @param string $key
     * @param mixed $value
     * @param string|null $module
     * @param string|null $filePath
     */
    public static function setConfig(string $key, $value, string $module = null, string $filePath = null): void
    {
        $configFileExistErrorMessage = "env.yml file doesn't exist.";
        $configKeyExistErrorMessage = $key . " configuration doesn't exist on env.yml.";

        if (!empty($module) && $module !== "app") {
            $configFileExistErrorMessage = "$module/$filePath.yml configuration file doesn't exist.";
            $configKeyExistErrorMessage = $key . " configuration doesn't exist on $module/$filePath.yml.";
        }

        try {
            $configFilePath = self::getConfigFilePath($module, $filePath);
            if (file_exists($configFilePath)) {
                $config = yaml_parse_file($configFilePath);

                $exist = self::keyGetValue($key, $config);

                if (!is_null($exist)) {
                    yaml_emit_file(
                        $configFilePath,
                        self::keySetValue($key, $value, $config),
                        YAML_UTF8_ENCODING,
                        YAML_CRLN_BREAK
                    );
                } else {
                    throw new ServerErrorException($configKeyExistErrorMessage);
                }
            } else {
                throw new ServerErrorException($configFileExistErrorMessage);
            }
        } catch (ServerErrorException $e) {
            self::logger($e->getMessage(), LogType::ERROR);
        }
    }

    /**
     * Return the current user
     *
     * @return Users|boolean
     */
    public static function getCurrentUser()
    {
        return CurrentUser::getInstance()->getCurrentUser();
    }

    /**
     * Get key value from recursive array.
     *
     * @param string $keyPath
     * @param array $config
     *
     * @return mixed|null
     */
    private static function keyGetValue(string $keyPath, array $config)
    {
        $keys = explode('.', $keyPath);

        foreach ($keys as $key) {
            $config = &$config[$key];
        }

        return $config;
    }

    /**
     * Set key value from recursive array.
     *
     * @param string $keyPath
     * @param mixed $value
     * @param array $config
     * @return array
     */
    private static function keySetValue(string $keyPath, $value, array $config): array
    {
        $keys = explode('.', $keyPath);

        $current = &$config;

        foreach ($keys as $key) {
            $current = &$current[$key];
        }

        $current = $value;

        return $config;
    }

    /**
     * Available languages list
     *
     * @return array
     */
    public static function availableLanguages(): array
    {
        return self::getConfig('languages', 'translation', 'languages');
    }

    /**
     * Get current language
     *
     * @return string
     */
    public static function currentLanguage(): string
    {
        return self::getConfig('site.language');
    }

    /**
     * Check if the language is available
     *
     * @param string $languageCode
     * @return bool
     */
    public static function isAllowedLanguage(string $languageCode): bool
    {
        return in_array($languageCode, self::availableLanguages());
    }

    /**
     * Get translation by context & key
     *
     * @param string $context
     * @param string $key
     * @param array $vars
     * @return string
     */
    public static function translate(string $context, string $key, array $vars = []): string
    {
        $translation = self::getConfig($key, 'translation', self::currentLanguage() . "." . $context);
        return strtr($translation, $vars);
    }
}
