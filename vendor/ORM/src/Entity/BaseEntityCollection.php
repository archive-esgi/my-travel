<?php

namespace ORM\Entity;

class BaseEntityCollection
{
    protected $entities;

    public function __construct(array $entities = [])
    {
        $this->entities = [];
        $this->addEntities($entities);
    }

    /**
     * Add an entity to the collection.
     *
     * @param \ORM\Entity\BaseEntity $newEntity
     */
    public function addEntity(BaseEntity $newEntity): void
    {
        foreach ($this->entities as $entity) {
            if ($entity === $newEntity) {
                return;
            }
        }

        $this->entities[] = $newEntity;
    }

    /**
     * Remove entity from the collection.
     *
     * @param int $id
     *
     * @return bool
     */
    public function removeEntity(int $id): bool
    {
        foreach ($this->entities as $key => $entity) {
            if ($entity->getId() === $id) {
                unset($this->entities[$key]);
                return true;
            }
        }

        return false;
    }

    /**
     * Get entity by id from the collection.
     *
     * @param int $id
     *
     * @return \ORM\Entity\BaseEntity
     */
    public function getEntity(int $id): BaseEntity
    {
        foreach ($this->entities as $entity) {
            if ($entity->getId() === $id) {
                return $entity;
            }
        }
    }

    /**
     * Get all entities of the collection.
     *
     * @return array
     */
    public function getEntities(): array
    {
        return $this->entities;
    }

    /**
     * Add many entities to the collection.
     *
     * @param array $entities
     */
    public function addEntities(array $entities): void
    {
        foreach ($entities as $entity) {
            $this->addEntity($entity);
        }
    }
}
