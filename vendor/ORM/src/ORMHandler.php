<?php

namespace ORM;

use ORM\Entity\BaseEntity;
use ORM\Entity\BaseEntityCollection;

trait ORMHandler
{
    /**
     * Get one entity by where clause.
     *
     * @param BaseEntity $entity
     * @param array $where
     * @return BaseEntity|boolean
     */
    public static function getOneEntityBy(BaseEntity $entity, array $where)
    {
        return $entity->getOneBy($where);
    }

    /**
     * Get all entities by where clause.
     *
     * @param BaseEntity $entity
     * @param array $where
     * @param int|null $limit
     * @param string|null $orderBy
     *
     * @return BaseEntityCollection
     */
    public static function getAllEntitiesBy(BaseEntity $entity, array $where, int $limit = null, string $orderBy = null)
    {
        return $entity->getAllBy($where, $limit, $orderBy);
    }

    /**
     * Get all entities.
     *
     * @param BaseEntity $entity
     * @param int|null $limit
     * @param string|null $orderBy
     *
     * @return BaseEntityCollection
     */
    public static function getAllEntities(BaseEntity $entity, int $limit = null, string $orderBy = null)
    {
        return $entity->getAll($limit, $orderBy);
    }

    /**
     * Delete entities by where clause.
     *
     * @param BaseEntity $entity
     * @param array $where
     * @return BaseEntityCollection
     */
    public static function deleteEntities(BaseEntity $entity, array $where)
    {
        return $entity->delete($where);
    }
}
