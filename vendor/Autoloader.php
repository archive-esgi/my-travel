<?php

namespace App;

/**
 * Class Autoloader
 * @package App
 */
class Autoloader
{

    /**
     * Load the searched class.
     *
     * @param string $className
     */
    public static function loader(string $className): void
    {
        $file = self::searchClass($className);

        if (!empty($file)) {
            require_once $file;
        }
    }

    /**
     * Search class by namespace.
     *
     * @param string $className
     *
     * @return string|null
     */
    private static function searchClass(string $className): ?string
    {
        // Get all sub namespace's
        $spaces = explode('\\', $className);

        // Base directory
        $baseDir = __DIR__ . '/';

        if (!file_exists($baseDir)) {
            return null;
        }

        // Generate file path
        $file = $baseDir;

        if ($spaces[0] == "App") {
            $file = str_replace('vendor/', '', $file);

            for ($i = 1; $i < count($spaces); $i++) {
                if ($i != count($spaces)-1) {
                    if ($i === 1) {
                        $file .= 'src/';
                    }

                    $file .= $spaces[$i] . '/';
                } else {
                    $file .= $spaces[$i] . '.php';
                }
            }
        } else {
            for ($i = 0; $i < count($spaces); $i++) {
                if ($i != count($spaces)-1) {
                    $file .= $spaces[$i] . '/';

                    if ($i === 0) {
                        $file .= 'src/';
                    }
                } else {
                    $file .= $spaces[$i] . '.php';
                }
            }
        }

        if (file_exists($file)) {
            return $file;
        }

        return null;
    }
}
