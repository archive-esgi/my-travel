ALTER DATABASE mytravel SET timezone TO 'Europe/Paris';

CREATE TABLE users (
    id serial PRIMARY KEY,
    firstname VARCHAR(50),
    lastname VARCHAR(100),
    username VARCHAR(50),
    email VARCHAR(150),
    password VARCHAR(60),
    description text,
    token VARCHAR(100),
    status VARCHAR(50),
    role VARCHAR(50),
    creation_date TIMESTAMP default NOW()::timestamp,
    update_date TIMESTAMP
);

CREATE TABLE trips (
    id serial PRIMARY KEY,
    user_id integer NOT NULL,
    title VARCHAR(150),
    description text,
    starting_date TIMESTAMP,
    ending_date TIMESTAMP,
    slug VARCHAR(200),
    image_path VARCHAR(150),
    creation_date TIMESTAMP default NOW()::timestamp,
    update_date TIMESTAMP,
    CONSTRAINT user_idFkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE steps (
    id serial PRIMARY KEY,
    trip_id integer NOT NULL,
    title VARCHAR(150),
    description text,
    longitude float,
    latitude float,
    starting_date TIMESTAMP,
    ending_date TIMESTAMP,
    creation_date TIMESTAMP default NOW()::timestamp,
    image_path VARCHAR(150),
    CONSTRAINT trip_idFkey FOREIGN KEY (trip_id) REFERENCES trips(id) ON DELETE CASCADE
);

CREATE TABLE tags (
    id serial PRIMARY KEY,
    label VARCHAR(150),
    creation_date TIMESTAMP default NOW()::timestamp
);

CREATE TABLE tripsTags (
    id serial PRIMARY KEY,
    trip_id integer NOT NULL,
    tag_id integer NOT NULL,
    CONSTRAINT trip_idFkey FOREIGN KEY (trip_id) REFERENCES trips(id) ON DELETE CASCADE,
    CONSTRAINT tag_idFkey FOREIGN KEY (tag_id) REFERENCES tags(id) ON DELETE CASCADE
);

CREATE TABLE reactions (
    id serial PRIMARY KEY,
    user_id integer NOT NULL,
    trip_id integer NOT NULL,
    CONSTRAINT user_idFkey FOREIGN KEY (user_id) REFERENCES users(id),
    CONSTRAINT trip_idFkey FOREIGN KEY (trip_id) REFERENCES trips(id)
);

CREATE TABLE media (
    id serial PRIMARY KEY,
    step_id integer NOT NULL,
    path VARCHAR(100),
    CONSTRAINT step_idFkey FOREIGN KEY (step_id) REFERENCES steps(id)
);

CREATE TABLE contacts (
    id serial PRIMARY KEY,
    user_id integer NOT NULL,
    subject text,
    content text,
    creation_date TIMESTAMP default NOW()::timestamp,
    CONSTRAINT user_idFkey FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE comments (
    id serial PRIMARY KEY,
    user_id integer NOT NULL,
    trip_id integer NOT NULL,
    text text,
    creation_date TIMESTAMP default NOW()::timestamp,
    update_date TIMESTAMP,
    CONSTRAINT user_idFkey FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT trip_idFkey FOREIGN KEY (trip_id) REFERENCES trips(id) ON DELETE CASCADE
);

CREATE TABLE themes (
    id serial PRIMARY KEY,
    name VARCHAR(100),
    slug VARCHAR(150),
    image_path VARCHAR(150),
    description text
);

CREATE TABLE locations (
    id serial PRIMARY KEY,
    name VARCHAR(100)
);

CREATE TABLE links (
    id serial PRIMARY KEY,
    location_id integer NOT NULL,
    name VARCHAR(100),
    url VARCHAR(150),
    CONSTRAINT location_idFkey FOREIGN KEY (location_id) REFERENCES locations(id)
);

CREATE TABLE pages (
    id serial PRIMARY KEY,
    name VARCHAR(50),
    title VARCHAR(150),
    slug VARCHAR(200),
    content text,
    location VARCHAR(50),
    creation_date TIMESTAMP default NOW()::timestamp,
    update_date TIMESTAMP
);