# MyTravel ✈️
![Version](https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000)

<div align="center">
    ![alt text](https://gitlab.com/bigburger/my-travel/raw/f0c4c22fa7ad374e962246201f5e87d3247a4c09/public/assets/images/logos/logo-primary.svg)
</div>

> MyTravel is a CMS that allows visitors to sign up to plan and share trips or road trips with other people. Users will be able to post pictures, their trip steps and the places they went to.

## Installation
Change folders rights so that the containers can run MyTravel

```sh
docker-compose up -d && chmod 777 env.yml && chmod -R 777 var && chmod -R 777  public/assets
```

## Authors

| <a href="https://gitlab.com/christophele" target="_blank">**Christophe LE**</a> | <a href="https://gitlab.com/myagoub" target="_blank">**Malek YABOUG**</a> | <a href="https://gitlab.com/alexiskn" target="_blank">**Alexis FAIZEAU**</a> | <a href="https://gitlab.com/AichaMEHDI" target="_blank">**Aicha MEHDI**</a>
| :---: |:---:|:---:| :---:|
| [![MyTravel](https://gitlab.com/uploads/-/system/user/avatar/1408322/avatar.png?width=200)](https://gitlab.com/christophele)    | [![MyTravel](https://gitlab.com/uploads/-/system/user/avatar/772438/avatar.png?width=200)](https://gitlab.com/myagoub) | [![MyTravel](https://secure.gravatar.com/avatar/55448a255c889a083abd8ee8a9b4083a?s=80&d=identicon)](https://gitlab.com/alexiskn)  | [![MyTravel](https://secure.gravatar.com/avatar/9f89492b565f2057d2bdf17c2af0e5ee?s=80&d=identicon)](https://gitlab.com/AichaMEHDI) |
| <a href="https://gitlab.com/christophele" target="_blank">`https://gitlab.com/christophele`</a> | <a href="https://gitlab.com/myagoub" target="_blank">`https://gitlab.com/myagoub`</a> | <a href="https://gitlab.com/alexiskn" target="_blank">`https://gitlab.com/alexiskn`</a> | <a href="https://gitlab.com/AichaMEHDI" target="_blank">`https://gitlab.com/AichaMEHDI`</a> |